@extends('layouts.loginapp')

@section('content')
<section class="bannr_se">
  <div id="" class="sldr">
    <li><img src="{{ asset('images/bannr2.png')}}" />
      <div class="ban_con">
        <h5>ROI Institute®, Inc., founded in 1992,
          helps organizations evaluate the success of projects
          and programs, including measuring the financial
          return on investment (ROI).</h5>
        <p style="text-align:left;">We do this by providing workshops, consulting, coaching, briefings and presentations, research, and benchmarking. ROI Institute operates through a network of partners and associates in the United States and in over 70 countries with the help of over 100 ROI consultants.</p>
        <div class="ban_btn"> <a href="">Contact Us Today</a> <a href="">Download Brochure</a> </div>
      </div>
    </li>
  </div>
</section>
<section class="main_cont">
  <div class="cor_cn_tr">
    <div class="container">
      <div class="abt_roi">
        <div class="abt_roi_lft">
          <h3>About The <strong>ROI Methodology</strong></h3>
          <p>The ROI Methodology is a scalable and systematic approach to program evaluation. Using a process model, five-level framework, and operating standards to capture performance metrics from simple satisfaction scores to financial impact, the methodology enables you to collect appropriate data to report performance of a variety of initiatives and program types. The ROI Methodology generates both qualitative and quantitative data and provides techniques to isolate the effects of the program from other influences–resulting in credible metrics and ROI reports accepted by financial executives and stakeholders.</p>
          <p>With over 6,000 organizations using this process, the ROI Methodology is the most used and implemented evaluation system in the world. The ROI Methodology not only provides the capability to evaluate program performance, but also improves the design of programs for optimal impact. A focused, proven and practical approach–the process is grounded in conservative standards and a cost effective approach to evaluation.</p>
        </div>
        <div class="abt_roi_rit">
          <h3><span>ROI Institute®</span> <strong>Our Mission</strong></h3>
          <ol>
            <li>To develop, refine, and support the use of the ROI Methodology in all types of applications and settings by building serious capability in individuals who become Certified ROI Professionals.</li>
            <li>To continually push to remain the global authority on measurement and evaluation of all types of projects and programs, including measuring the financial ROI.</li>
          </ol>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="who_do_we">
    <div class="container">
      <div class="who_do_we_con">
        <h3>About <strong>ROI Institute®</strong></h3>
        <p>ROI Institute, Inc., founded in 1992, helps organizations evaluate the success of projects and programs, including measuring the financial return on investment (ROI). We do this by providing workshops, consulting, coaching, hosting briefings and presentations, researching, and benchmarking. ROI Institute operates through a network of partners and associates in the U.S. and in over 70 countries with the help of over 100 ROI consultants. Our services are tailored to the following professional fields:</p>
      </div>
      <div class="abtsroinst">
        <ul>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig1.png')}}" /></span><strong>Human Resources / Human Capital</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig2.png')}}" /></span><strong>Training / Learning / Development</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig3.png')}}" /></span><strong>Leadership / Coaching / Mentoring</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig4.png')}}" /></span><strong>Knowledge Management / Transfer</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig5.png')}}" /></span><strong>Recognition / Incentives / Engagement</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig6.png')}}" /></span><strong>Change Management / Culture</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig7.png')}}" /></span><strong>Talent Management / Retention</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig8.png')}}" /></span><strong>Policies / Procedures / Processes</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig9.png')}}" /></span><strong>Technology / Systems / IT</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig10.png')}}" /></span><strong>Meetings / Events / Conferences</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig11.png')}}" /></span><strong>Marketing / Advertisement / Promotion</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig12.png')}}" /></span><strong>Organization Development / Consulting</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig13.png')}}" /></span><strong>Project Management Solutions</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig14.png')}}" /></span><strong>Quality / Six Sigma / Lean Engineering</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig15.png')}}" /></span><strong>Communications / Public Relations</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig16.png')}}" /></span><strong>Public Policy / Social Programs</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig17.png')}}" /></span><strong>Creativity / Innovation</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig18.png')}}" /></span><strong>Ethics / Compliance/Risk</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig19.png')}}" /></span><strong>Safety / Health / Fitness Programs</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig20.png')}}" /></span><strong>Green Projects / Sustainability</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig21.png')}}" /></span><strong>Healthcare Initiatives</strong></li>
          <li><span><img src="{{ asset('images/abt-roi-ins-ig22.png')}}" /></span><strong>Schools / Colleges / Universities</strong></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wt_we_ofr">
    <div class="container">
      <div class="wt_we_ofr_con">
        <h3>What <strong>We Offer</strong></h3>
      </div>
      <div id="parentHorizontalTab">
        <ul class="resp-tabs-list hor_1">
          <li>CONSULTING</li>
          <li>ROI CERTIFICATION®</li>
          <li>PUBLICATIONS / TOOLS</li>
        </ul>
        <div class="resp-tabs-container hor_1">
          <div>
            <p>ROI Certification is the most comprehensive program for gaining the knowledge, skills, and resources to evaluate programs of all types. It is a proven process to develop the capability necessary to measure the impact and ROI for any project or program. Upon completion, particpants receive continued support as they conduct their first impact study in their own organization. Upon demonstrating competency in the application of the ROI Methodology, participants earn the prestigious Certified ROI Professional® (CRP) designation, joining over 5,000 other professionals who have achieved this. No other program gives you access to the same level of expertise and capability as ROI Certification. This program has been approved for 32 (HR General) recertification credit hours toward PHR, SPHR and GPHR recertification through the HR Certification Institute.</p>
          </div>
          <div>
            <p>ROI Certification is the most comprehensive program for gaining the knowledge, skills, and resources to evaluate programs of all types. It is a proven process to develop the capability necessary to measure the impact and ROI for any project or program. Upon completion, particpants receive continued support as they conduct their first impact study in their own organization. Upon demonstrating competency in the application of the ROI Methodology, participants earn the prestigious Certified ROI Professional® (CRP) designation, joining over 5,000 other professionals who have achieved this. No other program gives you access to the same level of expertise and capability as ROI Certification.</p>
          </div>
          <div>
            <p>ROI Certification is the most comprehensive program for gaining the knowledge, skills, and resources to evaluate programs of all types. It is a proven process to develop the capability necessary to measure the impact and ROI for any project or program. Upon completion, particpants receive continued support as they conduct their first impact study in their own organization. Upon demonstrating competency in the application of the ROI Methodology, participants earn the prestigious Certified ROI Professional® (CRP) designation, joining over 5,000 other professionals who have achieved this. No other program gives you access to the same level of expertise and capability as ROI Certification. This program has been approved for 32 (HR General) recertification credit hours toward PHR, SPHR and GPHR recertification through the HR Certification Institute.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="roi_instu">
    <div class="container">
      <div class="roi_instu_con">
        <h3>Our Global <strong>Footprint</strong></h3>
        <p>ROI Institute® is proud to be in more than 70 countries all over the world. We work closely with partners to promote analytics and capability. For more information about a partnership with ROI Institute®, please send an email to <a href="mailto:info@roiinstitute.net">info@roiinstitute.net</a></p>
      </div>
      <div class="roi_in_caro" id="latest_news">
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
        <div class="roi_in_locn"><strong>Australia</strong>
          <p>Lever-Transfer of Learning</p>
        </div>
      </div>
    </div>
  </div>
  <div class="prtn_scn">
    <div class="container">
      <div class="prtn_scn_in">
        <div class="prtn_scn_hdn">
          <h3>ROI Has Worked With Over 5000 Companies</h3>
        </div>
        <div class="prtn_scn_in_slid">
          <ul>
            <li><img src="{{ asset('images/part1.png')}}" /></li>
            <li><img src="{{ asset('images/part2.png')}}" /></li>
            <li><img src="{{ asset('images/part3.png')}}" /></li>
            <li><img src="{{ asset('images/part4.png')}}" /></li>
            <li><img src="{{ asset('images/part5.png')}}" /></li>
            <li><img src="{{ asset('images/part6.png')}}" /></li>
            <li><img src="{{ asset('images/part7.png')}}" /></li>
            <li><img src="{{ asset('images/part8.png')}}" /></li>
            <li><img src="{{ asset('images/part9.png')}}" /></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="foll_sc">
    <div class="container">
      <div class="foll_sc_con">
        <h3>Get Connected <strong>FOLLOW US</strong></h3>
        <p>Get connected with us on social networks!</p>
      </div>
      <div class="socl_inc">
        <ul>
          <li><a href=""><img src="{{ asset('images/socl-im1.png')}}" /></a></li>
          <li><a href=""><img src="{{ asset('images/socl-im2.png')}}" /></a></li>
          <li><a href=""><img src="{{ asset('images/socl-im3.png')}}" /></a></li>
          <li><a href=""><img src="{{ asset('images/socl-im4.png')}}" /></a></li>
          <li><a href=""><img src="{{ asset('images/socl-im5.png')}}" /></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wht_th_clnt">
    <div class="container">
      <div class="wht_th_clnt_con">
        <h3>What Clients <strong>Say About Us</strong></h3>
      </div>
      <div class="wht_th_clnt_sc">
        <div class="wht_th_sc"> <img src="{{ asset('images/hw-co-img1.png')}}" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="{{ asset('images/hw-co-img2.png')}}" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="{{ asset('images/hw-co-img3.png')}}" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="{{ asset('images/hw-co-img4.png')}}" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection