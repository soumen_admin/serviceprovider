<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ config('app.name') }}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/owl_carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/sticky.js') }}" type="text/javascript"></script>
<script>
    $(window).load(function(){
      $("#header-nav").sticky({ topSpacing: 0 });
    });
  </script>
<script>(function($) {

  $.fn.menumaker = function(options) {
      
      var cssmenu = $(this), settings = $.extend({
        title: "Menu",
        format: "dropdown",
        sticky: false
      }, options);

      return this.each(function() {
        cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
        $(this).find("#menu-button").on('click', function(){
          $(this).toggleClass('menu-opened');
          var mainmenu = $(this).next('ul');
          if (mainmenu.hasClass('open')) { 
            mainmenu.hide().removeClass('open');
          }
          else {
            mainmenu.show().addClass('open');
            if (settings.format === "dropdown") {
              mainmenu.find('ul').show();
            }
          }
        });

        cssmenu.find('li ul').parent().addClass('has-sub');

        multiTg = function() {
          cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
          cssmenu.find('.submenu-button').on('click', function() {
            $(this).toggleClass('submenu-opened');
            if ($(this).siblings('ul').hasClass('open')) {
              $(this).siblings('ul').removeClass('open').hide();
            }
            else {
              $(this).siblings('ul').addClass('open').show();
            }
          });
        };

        if (settings.format === 'multitoggle') multiTg();
        else cssmenu.addClass('dropdown');

        if (settings.sticky === true) cssmenu.css('position', 'fixed');

        resizeFix = function() {
          if ($( window ).width() > 1024) {
            cssmenu.find('ul').show();
          }

          if ($(window).width() <= 1024) {
            cssmenu.find('ul').hide().removeClass('open');
          }
        };
        resizeFix();
        return $(window).on('resize', resizeFix);

      });
  };
})(jQuery);

(function($){
$(document).ready(function(){

$(document).ready(function() {
  $("#cssmenu").menumaker({
    title: "Menu",
    format: "multitoggle"
  });

  $("#cssmenu").prepend("<div id='menu-line'></div>");

var foundActive = false, activeElement, linePosition = 0, menuLine = $("#cssmenu #menu-line"), lineWidth, defaultPosition, defaultWidth;

$("#cssmenu > ul > li").each(function() {
  if ($(this).hasClass('active')) {
    activeElement = $(this);
    foundActive = true;
  }
});

if (foundActive === false) {
  activeElement = $("#cssmenu > ul > li").first();
}

defaultWidth = lineWidth = activeElement.width();

defaultPosition = linePosition = activeElement.position().left;

menuLine.css("width", lineWidth);
menuLine.css("left", linePosition);

$("#cssmenu > ul > li").hover(function() {
  activeElement = $(this);
  lineWidth = activeElement.width();
  linePosition = activeElement.position().left;
  menuLine.css("width", lineWidth);
  menuLine.css("left", linePosition);
}, 
function() {
  menuLine.css("left", defaultPosition);
  menuLine.css("width", defaultWidth);
});

});


});
})(jQuery);

</script>
<script type="text/javascript">
            $(document).ready(function () {
                $('body').click(function (e) {
                    var classClicked = e.target.className;
                    if (classClicked == 'on_click') {
                        $(".opne").toggle("700");
                    } else {
                        $(".opne").hide("700");
                    }
                });
            });
</script>
</head>
<body>
<section id="header-nav" class="header_sect">
  <div class="container">
    <div class="top_secn">
      <div class="top_secn_l"> visit our site <a href="https://www.roiinstitute.net" target="_blank">www.roiinstitute.net</a> </div>
      <div class="top_secn_r"> @guest <a href="{{ route('login') }}" class="long inlo">Login</a> @else <a href="{{ route('logout') }}" class="long outlog" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST"
          style="display: none;">
          @csrf
        </form>
        <div class="mnu_lg"> <a class="log_mne on_click"><img src="{{ asset('images/wit-prp.png') }}" alt="" /></a>
          <ul id="stat1" class="opne">
            <li><a href="{{route('user.profile',base64_encode(\Auth::user()->id))}}">user profile</a></li>
            <li><a href="{{route('user.user-course',base64_encode(\Auth::user()->id))}}">user course</a></li>
          </ul>
        </div>
        @endguest </div>
      <div class="clear"></div>
    </div>
    <div class="bmt_secn">
      <div class="logo_sc"><a href=""><img src="{{ asset('images/logo.png') }}" alt="" /></a></div>
      <div class="menu_sc">
        <div id="cssmenu">
          <ul>
            <li {!! \Route::current()->getName() == "about-us" ? "class='active'" : '' !!}><a href="{{route('about-us')}}">About</a></li>
            <li {!! \Route::current()->getName() == "course.list" ? "class='active'" : '' !!}><a href="{{route('course.list')}}">Courses</a></li>
            <li {!! \Route::current()->getName() == "workshop" ? "class='active'" : '' !!}><a href="{{route('workshop')}}">Workshops</a></li>
            <li {!! \Route::current()->getName() == "consulting" ? "class='active'" : '' !!}><a href="{{route('consulting')}}">Consulting</a></li>
            <li {!! \Route::current()->getName() == "history" ? "class='active'" : '' !!}><a href="{{route('history')}}">History</a></li>
            <li {!! \Route::current()->getName() == "certificate" ? "class='active'" : '' !!}><a href="{{route('certificate')}}">ROI Certification®</a></li>
            <li {!! \Route::current()->getName() == "awards" ? "class='active'" : '' !!}><a href="{{route('awards')}}">ROI Awards</a></li>
            <li {!! \Route::current()->getName() == "academy" ? "class='active'" : '' !!}><a href="{{route('academy')}}">Academy</a></li>
          </ul>
        </div>
        <div class="rit_mnu_log"><img src="{{ asset('images/menu-icon-rit.png') }}" /></div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<section class="bannr_se">
  <div id="" class="sldr">
    <li><img src="{{ asset('images/bannr1.png') }}" />
      <div class="ban_con">
        <h4>Grow Your</h4>
        <strong>Capabilities</strong>
        <p>Learn to demonstrate the relevance and value of your investments in people, programs, and projects through the application of the ROI Methodology.</p>
        <div class="ban_btn"> <a href="">Discover ROI Methodology</a> <a href="">Download Brochure</a> </div>
      </div>
    </li>
  </div>
</section>
<section class="main_cont">
  <div class="cor_cn_tr">
    <div class="container">
      <div class="cor_cn_tr_in">
        <div class="cor_cn_tr_sc_dv"><img src="{{ asset('images/cor-ic-1.png') }}" /><strong>Course<br />
          Calendar</strong>
          <p>Lorem Ipsum is simply dummy text of the print
            and typesetting industry.</p>
          <a href="{{route('course.list')}}">Show Calendar</a></div>
        <div class="cor_cn_tr_sc_dv"><img src="{{ asset('images/cor-ic-2.png') }}" /><strong>Trainee<br />
          Schedule</strong>
          <p>Lorem Ipsum is simply dummy text of the print
            and typesetting industry.</p>
          <a href="{{route('course.schedule')}}">Show Calendar</a></div>
        <div class="cor_cn_tr_sc_dv"><img src="{{ asset('images/cor-ic-3.png') }}" /><strong>ROI<br />
          Certification®</strong>
          <p>Lorem Ipsum is simply dummy text of the print
            and typesetting industry.</p>
          <a href="">Show Calendar</a></div>
      </div>
    </div>
  </div>
  <div class="who_do_we">
    <div class="container">
      <div class="who_do_we_con">
        <h3>What We Do <strong>Services Overview</strong></h3>
        <p>Looking to grow your capabilities? We conduct workshops and provide services all over the world
          to certify professionals in ROI.</p>
      </div>
      <div class="who_do_we_in">
        <div class="who_do_we_in_sc"> <img src="{{ asset('images/wt-do-icn1.png') }}" /> <strong>Teaching</strong>
          <p>Lorem Ipsum is simply dummy text..</p>
          <a href="">View more</a> </div>
        <div class="who_do_we_in_sc"> <img src="{{ asset('images/wt-do-icn2.png') }}" /> <strong>Consulting</strong>
          <p>Lorem Ipsum is simply dummy text..</p>
          <a href="">View more</a> </div>
        <div class="who_do_we_in_sc"> <img src="{{ asset('images/wt-do-icn3.png') }}" /> <strong>Research</strong>
          <p>Lorem Ipsum is simply dummy text..</p>
          <a href="">View more</a> </div>
        <div class="who_do_we_in_sc"> <img src="{{ asset('images/wt-do-icn4.png') }}" /> <strong>Writing</strong>
          <p>Lorem Ipsum is simply dummy text..</p>
          <a href="">View more</a> </div>
        <div class="who_do_we_in_sc"> <img src="{{ asset('images/wt-do-icn5.png') }}" /> <strong>Speaking</strong>
          <p>Lorem Ipsum is simply dummy text..</p>
          <a href="">View more</a> </div>
      </div>
    </div>
  </div>
  <div class="vdo_sc">
    <div class="vdo_sc_in">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/ImtZ5yENzgE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div class="tak_lok">
    <div class="container">
      <div class="tak_lok_con">
        <h3>Take a Look at <strong>Our Workshops</strong></h3>
        <p>Looking to grow your capabilities? We conduct workshops and provide services all over the world
          to certify professionals in ROI.</p>
      </div>
      <div class="tak_lok_con_in">
        <div class="tak_lok_con_dv"><img src="{{ asset('images/tak-lok1.png') }}" />
          <div class="tak_lok_con_dv_hv"> <strong>ROI Masterclass:<br />
            The Business Case<br />
            for Learning</strong>
            <p>5 September / 3 Oktober / 28 November</p>
            <a href="">Read more</a> </div>
        </div>
        <div class="tak_lok_con_dv"><img src="{{ asset('images/tak-lok2.png') }}" />
          <div class="tak_lok_con_dv_hv"> <strong>ROI Masterclass:<br />
            The Business Case<br />
            for Learning</strong>
            <p>5 September / 3 Oktober / 28 November</p>
            <a href="">Read more</a> </div>
        </div>
        <div class="tak_lok_con_dv"><img src="{{ asset('images/tak-lok3.png') }}" />
          <div class="tak_lok_con_dv_hv"> <strong>ROI Masterclass:<br />
            The Business Case<br />
            for Learning</strong>
            <p>5 September / 3 Oktober / 28 November</p>
            <a href="">Read more</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="prtn_scn">
    <div class="container">
      <div class="prtn_scn_in">
        <div class="prtn_scn_hdn">
          <h3>ROI Has Worked With Over 5000 Companies</h3>
        </div>
        <div class="prtn_scn_in_slid">
          <ul>
            <li><img src="{{ asset('images/part1.png') }}" /></li>
            <li><img src="{{ asset('images/part2.png') }}" /></li>
            <li><img src="{{ asset('images/part3.png') }}" /></li>
            <li><img src="{{ asset('images/part4.png') }}" /></li>
            <li><img src="{{ asset('images/part5.png') }}" /></li>
            <li><img src="{{ asset('images/part6.png') }}" /></li>
            <li><img src="{{ asset('images/part7.png') }}" /></li>
            <li><img src="{{ asset('images/part8.png') }}" /></li>
            <li><img src="{{ asset('images/part9.png') }}" /></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="about_sc">
    <div class="container">
      <div class="about_sc_inr">
        <h3>About ROI Institute® <strong>Consulting Services</strong></h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
        <a href="">Click Here to view more</a> </div>
    </div>
  </div>
  <div class="hire_sc">
    <div class="container">
      <div class="tak_lok_con">
        <h3>History of The <strong>ROI Institute®</strong></h3>
      </div>
      <div class="hire_inr_dv">
        <div class="hire_inr_dv_sc"> <img src="{{ asset('images/hire-img1.png') }}" />
          <div class="hire_inr_hd"><span>Founders</span> <strong>Jack</strong></div>
          <div class="hire_inr_con">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
          </div>
        </div>
        <div class="hire_inr_dv_sc"> <img src="{{ asset('images/hire-img2.png') }}" />
          <div class="hire_inr_hd"><span>Founders</span> <strong>Patti Phillips</strong></div>
          <div class="hire_inr_con">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="roi_instu">
    <div class="container">
      <div class="roi_instu_con">
        <h3>ROI Institute® <strong>Upcoming Events</strong></h3>
      </div>
      <div class="roi_in_caro" id="carousel_upcomCategory">
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
        <div class="roi_in_dve"> <img src="images/rio-inst-img1.png" /> <span>March 8 - March 12</span> <strong>ROI Certification — Berlin, Germany (Presented in Arabic)</strong>
          <p>ROI Certification is the most comprehensive way to gain the skills, resources..</p>
          <a href="">Read More</a> </div>
      </div>
    </div>
  </div>
  <div class="foll_sc">
    <div class="container">
      <div class="foll_sc_con">
        <h3>Get Connected <strong>FOLLOW US</strong></h3>
        <p>Get connected with us on social networks!</p>
      </div>
      <div class="socl_inc">
        <ul>
          <li><a href=""><img src="images/socl-im1.png" /></a></li>
          <li><a href=""><img src="images/socl-im2.png" /></a></li>
          <li><a href=""><img src="images/socl-im3.png" /></a></li>
          <li><a href=""><img src="images/socl-im4.png" /></a></li>
          <li><a href=""><img src="images/socl-im5.png" /></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wht_th_clnt">
    <div class="container">
      <div class="wht_th_clnt_con">
        <h3>What Clients <strong>Say About Us</strong></h3>
      </div>
      <div class="wht_th_clnt_sc">
        <div class="wht_th_sc"> <img src="images/hw-co-img1.png" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="images/hw-co-img2.png" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="images/hw-co-img3.png" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
        <div class="wht_th_sc"> <img src="images/hw-co-img4.png" />
          <div class="wht_th_sc_in">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
            <div class="fnfr"><strong>Travis William </strong> <span>Creative Director</span></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="footer">
  <div class="qryfme_sc">
    <div class="container">
      <div class="qryfme_sc_in">
        <div class="qryfme_sc_con">
          <h3>Get In Touch <strong>Contact Us</strong></h3>
          <p>Let your audience know when & where to say hello</p>
        </div>
        <div class="qryfme_sc_frm">
          <div class="qryfme_sc_frm_dv">
            <div class="qryfme_sc_frm_dv_inr">
              <div>
                <input name="" type="text" placeholder="Your Name" />
              </div>
              <div>
                <input name="" type="text" placeholder="Phone No." />
              </div>
            </div>
            <div>
              <input name="" type="text" placeholder="Email Id" />
            </div>
          </div>
          <div class="qryfme_sc_frm_dv">
            <textarea name="" cols="" rows="" placeholder="Message"></textarea>
          </div>
        </div>
        <button>Submit Your Info</button>
      </div>
    </div>
  </div>
  <div class="hv_mrque">
    <div class="container">
      <h3>Have More Questions? <strong>CONTACT US TODAY</strong></h3>
      <p><strong>+1 888 888 8888</strong></p>
      <p class="con_eml"><a href="mailto:info@roiinstitute.net">info@roiinstitute.net</a></p>
      <p>You can also visit our site <a href="www.roiinstitute.net" target="_blank">www.roiinstitute.net</a></p>
    </div>
  </div>
  <div class="foot_col">
    <div class="container">
      <div class="footr_dv">
        <div class="footer_qlinks">
          <h4>Quick Links</h4>
          <ul>
            <li> <a href="">About </a> </li>
            <li> <a href="#">Services</a></li>
            <li> <a href="#">Workshops</a></li>
            <li> <a  href="#">Consulting</a></li>
            <li> <a href="#">History</a></li>
            <li> <a href="#">ROI Certification</a></li>
            <li> <a href="#">ROI Awards</a></li>
            <li> <a  href="#">Academy</a></li>
            <li> <a href="#">Contact</a></li>
            <li> <a href="#">Privacy</a></li>
          </ul>
        </div>
        <div class="contact_scb">
          <h4>Workshops</h4>
          <p>We organize various incompany workshops throughout the year.<br />
            From analyzing reports to Business Alignment and ROI.</p>
          <p>ROI CERTIFICATION® PROGRAM MASTERCLASS</p>
          <a href="">View All Workshop</a> </div>
        <div class="foot_losec">
          <h4>Contact information</h4>
          <p>ROI Institute Europe<br />
            Groenekanseweg 85<br />
            3737 AC Groenekan, The Netherlands</p>
          <hr class="hrea_brd" />
          <p>Copyright © 2020 · ROI INSTITUTE® </p>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(function() {    
   $(document).on('click','.mnu_lg',function(){
      $(this).find('ul').css('display','block');      
    });

   $("#stat").mouseout(function(){
    $(this).css('display','none');
    //$(this).find('ul').css('display','none');
  });
});
</script> 
<script type="text/javascript"><!--
$('#carousel_featuredCategory').owlCarousel({
  items: 4,
  autoPlay: true,
  navigation: true,
  navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
  pagination: false
});
--></script> 
<script type="text/javascript"><!--
$('#custo_revw').owlCarousel({
  items: 1,
  autoPlay: true,
  navigation: false,
  navigationText: [''],
  pagination: false
});
--></script> 
<script type="text/javascript"><!--
$('#latest_news').owlCarousel({
  items: 3,
  autoPlay: true,
  navigation: true,
  navigationText: ['<i class="fa fa-chevron-left fa-5x"><img src="images/lft-aro.png" /></i>', '<i class="fa fa-chevron-right fa-5x"><img src="images/rigt-aro.png" /></i>'],
  pagination: false
});
--></script> 
<script type="text/javascript"><!--
$('#carousel_upcomCategory').owlCarousel({
  items: 3,
  autoPlay: true,
  navigation: true,
  navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
  pagination: false
});
--></script>
</body>
</html>
