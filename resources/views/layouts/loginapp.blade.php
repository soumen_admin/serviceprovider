<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ config('app.name') }}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" /> -->
<link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css" />
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/sticky.js') }}" type="text/javascript"></script>
<script>
    $(window).load(function(){
      $("#header-nav").sticky({ topSpacing: 0 });
    });
  </script>
<script type="text/javascript">
            $(document).ready(function () {
                $('body').click(function (e) {
                    var classClicked = e.target.className;
                    if (classClicked == 'on_click') {
                        $(".opne").toggle("700");
                    } else {
                        $(".opne").hide("700");
                    }
                });
            });
</script>
</head>
<body id="app">
<section id="header-nav" class="header_sect">
  <div class="container">
    <div class="top_secn">
      <div class="top_secn_l"> visit our site <a href="https://www.roiinstitute.net" target="_blank">www.roiinstitute.net</a> </div>
      <div class="top_secn_r"> @guest
        @if(\Route::current()->getName() == 'login') <a href="{{ route('register') }}" class="long inlo">Register</a> @else <a href="{{ route('login') }}" class="long inlo">Login</a> 
        <!-- <a class="nav-link long" href="{{ route('login') }}">{{ __('Login') }}</a> --> 
        @endif              
        @else <a href="{{ route('logout') }}" class="long" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST"
          style="display: none;">
          @csrf
        </form>
        <div class="mnu_lg"> <a class="log_mne on_click"><img src="{{ asset('images/wit-prp.png') }}" alt="" /></a>
          <ul id="stat1" class="opne">
            <li><a href="{{route('user.profile',base64_encode(\Auth::user()->id))}}">user profile</a></li>
            <li><a href="{{route('user.user-course',base64_encode(\Auth::user()->id))}}">user course</a></li>
          </ul>
        </div>
        @endguest </div>
      <div class="clear"></div>
    </div>
    <div class="bmt_secn">
      <div class="logo_sc"><a href="{{URL::to('/')}}"><img src="{{ asset('images/logo.png') }}" alt="" /></a></div>
      <div class="menu_sc">
        <div id="cssmenu">
          <ul>
            <li {!! \Route::current()->getName() == "about-us" ? "class='active'" : '' !!}><a href="{{route('about-us')}}">About</a></li>
            <li {!! \Route::current()->getName() == "course.list" ? "class='active'" : '' !!}><a href="{{route('course.list')}}">Courses</a></li>
            <li {!! \Route::current()->getName() == "workshop" ? "class='active'" : '' !!}><a href="{{route('workshop')}}">Workshops</a></li>
            <li {!! \Route::current()->getName() == "consulting" ? "class='active'" : '' !!}><a href="{{route('consulting')}}">Consulting</a></li>
            <li {!! \Route::current()->getName() == "history" ? "class='active'" : '' !!}><a href="{{route('history')}}">History</a></li>
            <li {!! \Route::current()->getName() == "certificate" ? "class='active'" : '' !!}><a href="{{route('certificate')}}">ROI Certification®</a></li>
            <li {!! \Route::current()->getName() == "awards" ? "class='active'" : '' !!}><a href="{{route('awards')}}">ROI Awards</a></li>
            <li {!! \Route::current()->getName() == "academy" ? "class='active'" : '' !!}><a href="{{route('academy')}}">Academy</a></li>
          </ul>
        </div>
        <div class="rit_mnu_log"><img src="{{ asset('images/menu-icon-rit.png') }}" /></div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
@yield('content')
<section class="footer"> @if(Route::current()->getName() == 'course.list' || Route::current()->getName() == 'register' || Route::current()->getName() == 'login' || Route::current()->getName() == 'password.request' || Route::current()->getName() == 'course.details' || Route::current()->getName() == 'course.training' || Route::current()->getName() == 'course.schedule' || Route::current()->getName() == 'user.profile' || Route::current()->getName() == 'user.user-course' || Route::current()->getName() == 'user.changepassword' || Route::current()->getName() == 'checkout')
  <link rel='stylesheet' href="{{ asset('css/fullcalendar.min.css') }}" />
  @else
  <div class="qryfme_sc">
    <div class="container">
      <div class="qryfme_sc_in">
        <div class="qryfme_sc_con">
          <h3>Get In Touch <strong>Contact Us</strong></h3>
          <p>Let your audience know when & where to say hello</p>
        </div>
        <div class="qryfme_sc_frm">
          <div class="qryfme_sc_frm_dv">
            <div class="qryfme_sc_frm_dv_inr">
              <div>
                <input name="" type="text" placeholder="Your Name" />
              </div>
              <div>
                <input name="" type="text" placeholder="Phone No." />
              </div>
            </div>
            <div>
              <input name="" type="text" placeholder="Email Id" />
            </div>
          </div>
          <div class="qryfme_sc_frm_dv">
            <textarea name="" cols="" rows="" placeholder="Message"></textarea>
          </div>
        </div>
        <button>Submit Your Info</button>
      </div>
    </div>
  </div>
  @endif
  <div class="hv_mrque">
    <div class="container">
      <h3>Have More Questions? <strong>CONTACT US TODAY</strong></h3>
      <p><strong>+1 888 888 8888</strong></p>
      <p class="con_eml"><a href="mailto:info@roiinstitute.net">info@roiinstitute.net</a></p>
      <p>You can also visit our site <a href="www.roiinstitute.net" target="_blank">www.roiinstitute.net</a></p>
    </div>
  </div>
  <div class="foot_col">
    <div class="container">
      <div class="footr_dv">
        <div class="footer_qlinks">
          <h4>Quick Links</h4>
          <ul>
            <li> <a href="">About </a> </li>
            <li> <a href="#">Services</a></li>
            <li> <a href="#">Workshops</a></li>
            <li> <a  href="#">Consulting</a></li>
            <li> <a href="#">History</a></li>
            <li> <a href="#">ROI Certification</a></li>
            <li> <a href="#">ROI Awards</a></li>
            <li> <a  href="#">Academy</a></li>
            <li> <a href="#">Contact</a></li>
            <li> <a href="#">Privacy</a></li>
          </ul>
        </div>
        <div class="contact_scb">
          <h4>Workshops</h4>
          <p>We organize various incompany workshops throughout the year.<br />
            From analyzing reports to Business Alignment and ROI.</p>
          <p>ROI CERTIFICATION® PROGRAM MASTERCLASS</p>
          <a href="">View All Workshop</a> </div>
        <div class="foot_losec">
          <h4>Contact information</h4>
          <p>ROI Institute Europe<br />
            Groenekanseweg 85<br />
            3737 AC Groenekan, The Netherlands</p>
          <hr class="hrea_brd" />
          <p>Copyright © 2020 · ROI INSTITUTE® </p>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</section>
@include('jquery')
</body>
</html>
