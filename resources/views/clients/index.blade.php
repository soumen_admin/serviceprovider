@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop
@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Client List</h3>
    </div>

    <div class="row">
        <div class="col-md-12"><a href="{{URL::route('clients_add')}}" class="btn btn-info pull-right createbtn"
                style="margin:15px;" id="create-new-branches">Add New</a></div>
    </div>

    <table class="table" id="example">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone No</th>
                <th scope="col">Address</th>
                <!-- <th scope="col">Status</th> -->
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
@stop
@section('js')
<script>
$(function() {
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            'url': '{{ route("clients") }}',
            'type': 'GET',
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'phoneNumber',
                name: 'phoneNumber'
            },
            {
                data: 'address',
                name: 'address'
            },            
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });

    $(document).on('click', '.delete_data', function() {
        $('#delete_modal').modal('show');

        var url = $(this).data('delete_url');
        $('#submit').attr('data-target_url', url);
    });

    $(document).on('click', '#submit', function() {
        var url = $(this).data('target_url');
        $('#delete_modal').modal('hide');
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(data) {
                window.location.reload();
            }
        });
    });

    //  AUTO ALERT CLOSE CODE 
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function() {
            $(this).remove();
        });
    }, 7000);
    //  AUTO ALERT CLOSE CODE 
});
</script>
<div class="modal" id="delete_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                <a href="javascript:void(0);" id="submit" data-target_url="" class="btn btn-primary">Yes</a>
            </div>
        </div>
    </div>
</div>
@stop