@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop
@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Page List</h3>
    </div>

    <div class="row">
        <div class="col-md-12"><a href="{{URL::route('page.create')}}" class="btn btn-info pull-right createbtn"
                style="margin:15px;">Add New</a></div>
    </div>

    <table class="table" id="example">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
@stop
@section('js')
<script>
$(function() {
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            'url': '{{ route("page.index") }}',
            'type': 'GET',
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'content',
                name: 'content'
            },
            {
                data: 'image',
                name: 'image'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },            
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },
        ]
    });

    $(document).on('click', '.delete_data', function() {
        $('#delete_modal').modal('show');

        var url = $(this).data('delete_url');
        var delete_id = $(this).data('delete_id');
        $('#submit').attr('data-target_url', url);
        $('#submit').attr('data-delete_id', delete_id);
    });

    $(document).on('click', '#submit', function() {
        var url = $(this).attr('data-target_url');
        var rowid = $(this).attr('data-delete_id');
        $('#delete_modal').modal('hide');
        $.ajax({
            type: 'delete',
            url: url,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(data) {
                var row = document.getElementById(data.dataId);
                row.parentElement.removeChild(row);
                //window.location.reload();
            }
        });
    });    
});
</script>
<div class="modal" id="delete_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" id="submit" data-delete_id="" data-target_url="" class="btn btn-danger">Yes</a>
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
            </div>
        </div>
    </div>
</div>
@stop