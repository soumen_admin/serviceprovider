@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Edit page</h1>
@stop
@section('content')

    {!!
    Form::open(array('route'=>array('page.update',base64_encode($page->id)),'class'=>'form-horizontal','method'=>'put',
    'role'=>'form', 'files' => true)) !!}    
    <div>   
        <div class="box-body mt-4">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="widget-content">
                        <fieldset>
                            <div class="row">

                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label class="control-label" for="title">Name</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="page Name" name="title" class="span3" id="title"
                                                value="{{ $page->title }}" required autofocus />
                                            @if ($errors->has('title')) <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('content') ? ' has-error' : '' }}">
                                        <label class="control-label" for="content">Description</label>
                                        <div class="controls">
                                            <textarea class="form-control my-editor" name="content"  id="my-editor" rows="5" value="{{ old('content') }}" required autofocus>{{ $page->content }}
                                                </textarea>
                                            @if ($errors->has('content')) <span class="help-block">
                                                <strong>{{ $errors->first('content') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-7">
                                    <div class="control-group required{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <label class="control-label" for="image">File</label>
                                        <div class="controls">
                                            <input type="file" maxlength="100" class="form-control" placeholder="page Venue"
                                                name="image" class="span3" id="image" value="{{ old('image') }}"
                                                autofocus />
                                            @if ($errors->has('image')) <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong> </span> @endif </div>
                                    </div>
                                </div>                   

                               <div class="col-xs-10 col-sm-7" style="margin-top:45px;">
                                    <div class="form-actions">
                                    <input type="submit" class="btn btn-primary" value="Update" />
                                    <a onclick="window.history.go(-1); return false;" class="btn btn-danger">Cancel</a> </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
      </div>
    </div>
    {!! Form::close() !!}
    

@stop