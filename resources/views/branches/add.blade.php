@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Add branches</h1>
@stop
@section('content')

{!! Form::open(array('route'=>'branches_add','class'=>'form-horizontal','method'=>'post', 'role'=>'form')) !!}

<div>    
    <div class="box-body">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="widget-content">
                        <fieldset>
                            <div class="row">

                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div
                                        class="control-group required{{ $errors->has('firstName') ? ' has-error' : '' }}">
                                        <label class="control-label" for="firstName">First Name</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="firstName" name="firstName" class="span3" id="firstName"
                                                value="{{ old('firstName') }}" required autofocus />
                                            @if ($errors->has('firstName')) <span class="help-block">
                                                <strong>{{ $errors->first('firstName') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div
                                        class="control-group required{{ $errors->has('lastName') ? ' has-error' : '' }}">
                                        <label class="control-label" for="lasttName">Last Name</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="lastName" name="lastName" class="span3" id="lastName"
                                                value="{{ old('lastName') }}" required autofocus />
                                            @if ($errors->has('lastName')) <span class="help-block">
                                                <strong>{{ $errors->first('lastName') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div class="control-group required{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label" for="email">Email Address</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control" placeholder="email"
                                                name="email" class="span3" id="email" value="{{ old('email') }}"
                                                required autofocus />
                                            @if ($errors->has('email')) <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div class="control-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label" for="password">Password</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="Password" name="password" id="password"
                                                rvalue="{{ old('password') }}" required autofocus />
                                            @if ($errors->has('password')) <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div
                                        class="control-group required{{ $errors->has('phoneNumber') ? ' has-error' : '' }}">
                                        <label class="control-label" for="phoneNumber">Phone</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="phoneNumber"
                                                name="phoneNumber" class="span3" id="phoneNumber"
                                                value="{{ old('phoneNumber') }}" required autofocus />
                                            @if ($errors->has('phoneNumber')) <span class="help-block">
                                                <strong>{{ $errors->first('phoneNumber') }}</strong> </span> @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div
                                        class="control-group required{{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label class="control-label" for="country">Country</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="country" name="country"
                                                class="span3" id="country" value="{{ old('country') }}" required
                                                autofocus />
                                            @if ($errors->has('country')) <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div class="control-group required{{ $errors->has('state') ? ' has-error' : '' }}">
                                        <label class="control-label" for="state">State</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="state" name="state"
                                                class="span3" id="state" value="{{ old('state') }}" required
                                                autofocus />
                                            @if ($errors->has('state')) <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                                    <div class="control-group required{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label class="control-label" for="city">City</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="city" name="city"
                                                class="span3" id="city" value="{{ old('city') }}" required autofocus />
                                            @if ($errors->has('city')) <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong> </span> @endif </div>
                                    </div>
                                </div>



                                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                    <div
                                        class="control-group required{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label class="control-label" for="Address">Address</label>
                                        <div class="controls">
                                            <textarea class="form-control" placeholder="Address"
                                                style="height:100px;width:100%" onblur="return copyaddress(this.value)"
                                                name="address" id="address" rows="3">{{old('address')}}</textarea required>
                              @if ($errors->has('address')) <span class="help-block"> <strong>{{ $errors->first('address') }}</strong> </span> @endif </div> 
                        </div>
                    </div>
                    <div class="col-xs-10 col-sm-6 col-md-6 col-lg-6">
                        <div class="control-group required{{ $errors->has('status') ? ' has-error' : '' }}">
                          <label class="control-label" for="status">Status</label>
                          <div class="controls">
                            <select name="status" class="custom-select">
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                            </select>
                            @if ($errors->has('status')) <span class="help-block"> <strong>{{ $errors->first('status') }}</strong> </span> @endif </div>
                        </div>
                    </div>
                    <div class="col-md-3 mt-sm-3" style="margin-top:45px;">
                        <div class="form-actions">
                        <input type="submit" class="btn btn-primary" onclick="return check_lease_validation()" value="Save" />
                        <a onclick="window.history.go(-1); return false;" class="btn btn-danger">Cancel</a> </div>
                    </div>
              </div>
           </fieldset>
          </div>
        </div>
      </div>
      </div>
  </div>
{!! Form::close() !!}
@stop