<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/responsiveslides.min.js') }}"></script>
<script src="{{ asset('js/owl_carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/sticky.js') }}" type="text/javascript"></script>
<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
</script>
<script>
    $(window).load(function(){
      $("#header-nav").sticky({ topSpacing: 0 });
    });
  </script>
<script>
    $(window).load(function(){
      $("#header-nav-inr").sticky({ topSpacing: 0 });
    });
  </script>
<script>

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});



</script>
<script type="text/javascript">

  $(function() {    

   $(document).on('click','.mnu_lg',function(){

      $(this).find('ul').css('display','block');      

    });



   $("#stat").mouseout(function(){

    $(this).css('display','none');

    //$(this).find('ul').css('display','none');

  });

});

</script>

@if(Route::current()->getName() == 'course.training') 
<script type="text/javascript">

    $(document).ready(function() {            

    // page is now ready, initialize the calendar...            
    $('#calendar').fullCalendar({

        // put your options and callbacks here

        plugins: [ 'dayGrid', 'interaction' ],

        header: {

            left: 'prev,next today',

            center: 'title',

            right: 'month,agendaWeek,agendaDay,listWeek'

        },

        defaultDate:moment().format("YYYY-MM-DD"),                

        editable: true,                

        events : [

            @if(count($course[0]['schedule']) > 0)

                @foreach($course[0]['schedule'] as $dat)

                {

                    title : '{{ $course[0]->cName}}  At {{$dat->start_time}}',

                    start : '{{ $dat->start_date }}',                    

                    id    : '{{ $course[0]->id.'-'.$dat->id }}',

                    description:'{{$dat->availableSeat}}'

                },

                @endforeach

            @endif

        ],        

        eventMouseover: function (data, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'Seat Available ' + ': ' + data.description + '</div>';

            $("body").append(tooltip);

            $(this).mouseover(function (e) {

                $(this).css('z-index', 10000);

                $('.tooltiptopicevent').fadeIn('500');

                $('.tooltiptopicevent').fadeTo('10', 1.9);

            }).mousemove(function (e) {

                $('.tooltiptopicevent').css('top', e.pageY + 10);

                $('.tooltiptopicevent').css('left', e.pageX + 20);

            });

        },

        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').remove();
        },

        eventClick: function(calEvent, jsEvent, view) {
          if(calEvent.description > 0){
            var id = calEvent.id.split("-");
            var token = btoa(id[0]);
            var token1 = btoa(id[1]);
            window.location.href = APP_URL+"/checkout/"+token+"/"+token1+"";  
          }
          else{
              alert("Seat is not Available.");
              return false;
          }          
        }
    });            

});

</script> 
@endif



@if(Route::current()->getName() == 'course.schedule') 
<script type="text/javascript">

    $(document).ready(function() {            

    // page is now ready, initialize the calendar...            

    $('#calendar').fullCalendar({

        // put your options and callbacks here

        plugins: [ 'dayGrid', 'interaction' ],

        header: {

            left: 'prev,next today',

            center: 'title',

            right: 'month,agendaWeek,agendaDay,listWeek'

        },

        defaultDate:moment().format("YYYY-MM-DD"),                

        editable: true,                

        events : [

            @if(count($courses) > 0)

                @foreach($courses as $course)

                {

                    title : '{{ $course->cName}}',

                    start : '{{ $course->start_date }}T{{ $course->start_time }}',

                    description: '{{ $course->availableSeat }}',

                    id    : '{{ $course->id }}'                    

                },

                @endforeach

            @endif

        ],        

        eventMouseover: function (data, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#feb811;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'Seat Available ' + ': ' + data.description + '</div>';

            $("body").append(tooltip);

            $(this).mouseover(function (e) {

                $(this).css('z-index', 10000);

                $('.tooltiptopicevent').fadeIn('500');

                $('.tooltiptopicevent').fadeTo('10', 1.9);

            }).mousemove(function (e) {

                $('.tooltiptopicevent').css('top', e.pageY + 10);

                $('.tooltiptopicevent').css('left', e.pageX + 20);

            });

        },

        eventMouseout: function (data, event, view) {

            $(this).css('z-index', 8);



            $('.tooltiptopicevent').remove();



        },

    });            

});

</script> 
@endif 
<script type="text/javascript">

(function($) {



  $.fn.menumaker = function(options) {

      

      var cssmenu = $(this), settings = $.extend({

        title: "Menu",

        format: "dropdown",

        sticky: false

      }, options);



      return this.each(function() {

        cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');

        $(this).find("#menu-button").on('click', function(){

          $(this).toggleClass('menu-opened');

          var mainmenu = $(this).next('ul');

          if (mainmenu.hasClass('open')) { 

            mainmenu.hide().removeClass('open');

          }

          else {

            mainmenu.show().addClass('open');

            if (settings.format === "dropdown") {

              mainmenu.find('ul').show();

            }

          }

        });



        cssmenu.find('li ul').parent().addClass('has-sub');



        multiTg = function() {

          cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');

          cssmenu.find('.submenu-button').on('click', function() {

            $(this).toggleClass('submenu-opened');

            if ($(this).siblings('ul').hasClass('open')) {

              $(this).siblings('ul').removeClass('open').hide();

            }

            else {

              $(this).siblings('ul').addClass('open').show();

            }

          });

        };



        if (settings.format === 'multitoggle') multiTg();

        else cssmenu.addClass('dropdown');



        if (settings.sticky === true) cssmenu.css('position', 'fixed');



        resizeFix = function() {

          if ($( window ).width() > 1024) {

            cssmenu.find('ul').show();

          }



          if ($(window).width() <= 1024) {

            cssmenu.find('ul').hide().removeClass('open');

          }

        };

        resizeFix();

        return $(window).on('resize', resizeFix);



      });

  };

})(jQuery);



(function($){

$(document).ready(function(){



$(document).ready(function() {

  $("#cssmenu").menumaker({

    title: "Menu",

    format: "multitoggle"

  });



  $("#cssmenu").prepend("<div id='menu-line'></div>");



var foundActive = false, activeElement, linePosition = 0, menuLine = $("#cssmenu #menu-line"), lineWidth, defaultPosition, defaultWidth;



$("#cssmenu > ul > li").each(function() {

  if ($(this).hasClass('active')) {

    activeElement = $(this);

    foundActive = true;

  }

});



if (foundActive === false) {

  activeElement = $("#cssmenu > ul > li").first();

}



defaultWidth = lineWidth = activeElement.width();



defaultPosition = linePosition = activeElement.position().left;



menuLine.css("width", lineWidth);

menuLine.css("left", linePosition);



$("#cssmenu > ul > li").hover(function() {

  activeElement = $(this);

  lineWidth = activeElement.width();

  linePosition = activeElement.position().left;

  menuLine.css("width", lineWidth);

  menuLine.css("left", linePosition);

}, 

function() {

  menuLine.css("left", defaultPosition);

  menuLine.css("width", defaultWidth);

});



});





});

})(jQuery);



</script> 
<script type="text/javascript"><!--

$('#carousel_featuredCategory').owlCarousel({

  items: 4,

  autoPlay: true,

  navigation: true,

  navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],

  pagination: false

});

--></script> 
<script type="text/javascript"><!--

$('#custo_revw').owlCarousel({

  items: 1,

  autoPlay: true,

  navigation: false,

  navigationText: [''],

  pagination: false

});

--></script> 
<script type="text/javascript"><!--

$('#latest_news').owlCarousel({

  items: 3,

  autoPlay: true,

  navigation: true,

  navigationText: ['<i class="fa fa-chevron-left fa-5x"><img src="images/lft-aro.png" /></i>', '<i class="fa fa-chevron-right fa-5x"><img src="images/rigt-aro.png" /></i>'],

  pagination: false

});

--></script> 
<script type="text/javascript"><!--

$('#carousel_upcomCategory').owlCarousel({

  items: 3,

  autoPlay: true,

  navigation: true,

  navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],

  pagination: false

});

--></script> 
<script type="text/javascript">

    $(document).ready(function(){

        $(document).on('click',"#ert",function(){

         content = $("#content").val();

         dateSearch = $("#dateSearch").val();



         $.ajax({

            type:'Post',

            url:'{{route("course.courselist")}}',

            data:{dateSearch : dateSearch , content : content},

            dataType:'json',

            success:function(data){

                $(".cour_dtl_in_sec").html(data.structure);

            }

         });

        })

    })

</script>