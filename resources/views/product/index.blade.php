@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

@stop
@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Product List</h3>
    </div>
    <div class="row">
        <div class="form-group col-md-4">
            <h5>Start Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> 
                <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group col-md-4">
            <h5>End Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div>
            </div>
        </div>
        <div class="form-group col-md-4 text-left" style="margin-top: 35px;">
            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12"><a href="{{URL::route('product.create')}}" class="btn btn-info pull-right createbtn"
                style="margin:15px;">Add New</a></div>
    </div>

    <table class="table" id="example">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Price per unit</th>
                <th scope="col">Quantity</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
@stop
@section('js')
<script>
$(function() {
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            'url': '{{ route("product.index") }}',
            'type': 'GET',            
            data: function (d) {
              d.start_date = $('#start_date').val();
              d.end_date = $('#end_date').val();
            },
            'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {   data: 'pName',name: 'pName' },
            {   data: 'pDescription',name: 'pDescription' },
            {   data: 'pPrice',name: 'pPrice' },
            {   data: 'pQuantity',name: 'pQuantity' },
            {   data: 'created_at',name: 'created_at' },
            {   data: 'action',name: 'action', orderable: false,searchable: false },
        ]
    });

    $('#btnFiterSubmitSearch').click(function(){
        $('#example').DataTable().draw(true);
    });

    $(document).on('click', '.delete_data', function() {
        $('#delete_modal').modal('show');

        var url = $(this).data('delete_url');
        var delete_id = $(this).data('delete_id');
        $('#submit').attr('data-target_url', url);
        $('#submit').attr('data-delete_id', delete_id);
    });

    $(document).on('click', '#submit', function() {
        var url = $(this).attr('data-target_url');
        var rowid = $(this).attr('data-delete_id');
        $('#delete_modal').modal('hide');
        $.ajax({
            type: 'delete',
            url: url,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(data) {
                var row = document.getElementById(data.dataId);
                row.parentElement.removeChild(row);
                //window.location.reload();
            }
        });
    });    
});
</script>
<div class="modal" id="delete_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" id="submit" data-delete_id="" data-target_url="" class="btn btn-danger">Yes</a>
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
            </div>
        </div>
    </div>
</div>
@stop