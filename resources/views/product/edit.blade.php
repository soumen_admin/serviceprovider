@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Edit Product</h1>
@stop
@section('content')

    {!!
    Form::open(array('route'=>array('product.update',base64_encode($product->id)),'class'=>'form-horizontal','method'=>'put',
    'role'=>'form','files'=>true)) !!}    
    <div>   
        <div class="box-body mt-4">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="widget-content">
                        <fieldset>
                            <div class="row">

                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('pName') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pName">Name</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="product Name" name="pName" class="span3" id="pName"
                                                value="{{ $product->pName }}" required autofocus />
                                            @if ($errors->has('pName')) <span class="help-block">
                                                <strong>{{ $errors->first('pName') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('pDescription') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pDescription">Description</label>
                                        <div class="controls">
                                            <textarea class="form-control my-editor" name="pDescription"  id="pDescription" rows="5" required autofocus>{{ $product->pDescription }}
                                                </textarea>
                                            @if ($errors->has('pDescription')) <span class="help-block">
                                                <strong>{{ $errors->first('pDescription') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-7">
                                    <div class="control-group required{{ $errors->has('pPrice') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pPrice">Price</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="product Venue"
                                                name="pPrice" class="span3" id="pPrice" value="{{ $product->pPrice }}"
                                                required autofocus />
                                            @if ($errors->has('pPrice')) <span class="help-block">
                                                <strong>{{ $errors->first('pPrice') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-7">
                                    <div class="control-group required{{ $errors->has('pQuantity') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pQuantity">Quantity</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="product Venue"
                                                name="pQuantity" class="span3" id="pQuantity" value="{{ $product->pQuantity }}"
                                                required autofocus />
                                            @if ($errors->has('pQuantity')) <span class="help-block">
                                                <strong>{{ $errors->first('pQuantity') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                               <div class="col-xs-10 col-sm-7" style="margin-top:45px;">
                                    <div class="form-actions">
                                    <input type="submit" class="btn btn-primary" value="Update" />
                                    <a onclick="window.history.go(-1); return false;" class="btn btn-danger">Cancel</a> </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
      </div>
    </div>
    {!! Form::close() !!}
    

@stop