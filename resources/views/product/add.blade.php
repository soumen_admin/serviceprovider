@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Add product</h1>
@stop
@section('content')

{!! Form::open(array('route'=>'product.store','class'=>'form-horizontal','method'=>'post', 'role'=>'form', 'files'=> true)) !!}
<div>   
    <div class="box-body mt-4">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="widget-content">
                        <fieldset>
                            <div class="row">

                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('pName') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pName">Name</label>
                                        <div class="controls">
                                            <input type="text" maxlength="100" class="form-control"
                                                placeholder="product Name" name="pName" class="span3" id="pName"
                                                value="{{ old('pName') }}" required autofocus />
                                            @if ($errors->has('pName')) <span class="help-block">
                                                <strong>{{ $errors->first('pName') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('pDescription') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pDescription">Description</label>
                                        <div class="controls">
                                            <textarea class="form-control my-editor" name="pDescription"  id="pDescription" rows="5" value="{{ old('pDescription') }}" required autofocus>
                                                </textarea>
                                            @if ($errors->has('pDescription')) <span class="help-block">
                                                <strong>{{ $errors->first('pDescription') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-7">
                                    <div class="control-group required{{ $errors->has('pPrice') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pPrice">Price</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="product Venue"
                                                name="pPrice" class="span3" id="pPrice" value="{{ old('pPrice') }}"
                                                required autofocus />
                                            @if ($errors->has('pPrice')) <span class="help-block">
                                                <strong>{{ $errors->first('pPrice') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-7">
                                    <div class="control-group required{{ $errors->has('pQuantity') ? ' has-error' : '' }}">
                                        <label class="control-label" for="pQuantity">Quantity</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" placeholder="product Venue"
                                                name="pQuantity" class="span3" id="pQuantity" value="{{ old('pQuantity') }}"
                                                required autofocus />
                                            @if ($errors->has('pQuantity')) <span class="help-block">
                                                <strong>{{ $errors->first('pQuantity') }}</strong> </span> @endif </div>
                                    </div>
                                </div>


                                <!-- <div class="col-xs-10 col-sm-7">
                                    <div class="control-group{{ $errors->has('cThumbnail') ? ' has-error' : '' }}">
                                        <label class="control-label" for="cThumbnail">Thumbnail</label>
                                        <div class="controls">
                                            <input type="file" class="form-control"
                                                placeholder="product Price" name="cThumbnail" id="cThumbnail"
                                                autofocus />
                                            @if ($errors->has('cThumbnail')) <span class="help-block">
                                                <strong>{{ $errors->first('cThumbnail') }}</strong> </span> @endif </div>
                                    </div>
                                </div>

                                <div class="col-xs-10 col-sm-7">
                                    <div
                                        class="control-group required{{ $errors->has('cFile') ? ' has-error' : '' }}">
                                        <label class="control-label" for="cFile">File</label>
                                        <div class="controls">
                                            <input class="date form-control"  type="file" name="cFile" id="cFile">
                                            @if ($errors->has('cFile')) <span class="help-block">
                                                <strong>{{ $errors->first('cFile') }}</strong> </span> @endif
                                        </div>
                                    </div>
                                </div> -->

                               <div class="col-xs-10 col-sm-7" style="margin-top:45px;">
                                    <div class="form-actions">
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                    <a onclick="window.history.go(-1); return false;" class="btn btn-danger">Cancel</a> </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
      </div>
  </div>
{!! Form::close() !!}
@stop
