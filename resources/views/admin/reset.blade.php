@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>{{ trans('adminlte::adminlte.password_reset_message') }}</h1>
@stop
@section('content')
@php
	$email = Auth::user()->email;
@endphp
<div>   
    <div class="box-body mt-4">
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="widget-content">
        				<div class="row">            				            
				            <form action="{{ route('admin_password') }}" method="post">
				                 @csrf                 
				                <div class="col-xs-10 col-sm-7">
					                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
					                    <input type="email" name="email" class="form-control" value="{{ isset($email) ? $email : old('email') }}"
					                           placeholder="{{ trans('adminlte::adminlte.email') }}">
					                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					                    @if ($errors->has('email'))
					                        <span class="help-block">
					                            <strong>{{ $errors->first('email') }}</strong>
					                        </span>
					                    @endif
					                </div>
				        		</div>
				        		<div class="col-xs-10 col-sm-7">
					                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
					                    <input type="password" name="password" class="form-control"
					                           placeholder="{{ trans('adminlte::adminlte.password') }}">
					                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
					                    @if ($errors->has('password'))
					                        <span class="help-block">
					                            <strong>{{ $errors->first('password') }}</strong>
					                        </span>
					                    @endif
					                </div>
					            </div>
					            <div class="col-xs-10 col-sm-7">
					                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
					                    <input type="password" name="password_confirmation" class="form-control"
					                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
					                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
					                    @if ($errors->has('password_confirmation'))
					                        <span class="help-block">
					                            <strong>{{ $errors->first('password_confirmation') }}</strong>
					                        </span>
					                    @endif
					                </div>
					            </div>
					            <div class="col-xs-10 col-sm-7" style="margin-top:45px;">
					                <button type="submit" class="btn btn-primary btn-block btn-flat">
					                    {{ trans('adminlte::adminlte.reset_password') }}
					                </button>
					            </div>
				            </form>
				        </div>
				    </div>
				</div>				
			</div>
        </div>
    </div>
</div><!-- /.login-box -->
@stop
