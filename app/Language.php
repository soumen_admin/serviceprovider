<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
     protected $fillable = [
        'id', 'lang_name', 'code','status'
    ];
}
