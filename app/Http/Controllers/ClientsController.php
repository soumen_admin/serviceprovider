<?php

namespace App\Http\Controllers;

use App\User;
use App\Language;
use Illuminate\Http\Request;
use DataTables;
use DB;
use \Session, \Validator,\Redirect;

class ClientsController extends Controller
{
    //

    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $data = User::select(DB::raw("CONCAT(firstName,' ',lastName) AS name"),'id','email', 'phoneNumber', 'address', DB::raw('(CASE
                        WHEN status = "0" THEN "Inactive" 
                        WHEN users.status = "1" THEN "Active" END) AS status'))->where('firstname','!=','admin')->where('is_user',2);

            return Datatables::of($data)
            
           ->filterColumn('name', function($query, $keyword) {
                $query->whereRaw("CONCAT(users.firstName,users.lastName) like ?", ["%{$keyword}%"]);
            })

            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<a title="Edit" href="'.route("clients_edit",base64_encode($user->id)).'" class="btn btn-xs btn-success fa fa-edit"></a>&nbsp;<a href="javascript:void(0);" title="Delete" data-delete_url="'.route("clients_delete",base64_encode($user->id)).'" data-delete_id = "'.base64_encode($user->id).'" class="btn btn-xs btn-danger delete_data fa fa-trash"></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
            $query = $dataTable->getQuery()->get();
        }
        return view('clients.index');    	
    }

    public function delete(Request $request,$id){
    	$user = User::findOrFail(base64_decode($id));
  		$user->delete();
		$request->session()->put('success', 'Clients Data Deleted Successfully');
		return response()->json(['status'=>1]);    	
    
    }

    public function show(){
        return view('clients.add');
    }

    public function store(Request $request){ 
        $inputs = $request->all();       
        $validator = Validator::make($inputs,
        [
            'firstName'     => 'required',
            'lastName'      => 'required',
            'email'         => 'sometimes|email|max:255',
            'password'      => 'required|max:20',    
            'phoneNumber'   => 'required|max:11', 
            'country'       => 'required',
            'state'         => 'required',
            'address'       => 'required'  
        ]);
                    
        if($validator->fails()){
            $message = $validator->messages();                        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
        
            $user = new User([
                'firstName'         => $inputs['firstName'],
                'lastName'          => $inputs['lastName'],
                'email'             => $inputs['email'],
                'password'          => bcrypt($inputs['password']),
                'phoneNumber'       => $inputs['phoneNumber'],
                'country'           => $inputs['country'],
                'state'             => $inputs['state'],
                'city'              => $inputs['city'],
                'address'           => $inputs['address'],
                'is_user'           => 2,
                'status'            => $inputs['status'],                
                'created_at'        => date('Y-m-d H:i:s')             
            ]);                 
            $user->save();
            return redirect('/clients')->with('success', 'Clients Master Data Added Successfully');
        }
    }

    public function edit($uid){
        $uid=base64_decode($uid);
        $clients = User::find($uid);
        return view('clients.edit', compact('clients'));
    }


    public function update($uid, Request $request){ 
        $inputs = $request->all();
                
        $validator = Validator::make($inputs,
        [
            'firstName'     => 'required',
            'lastName'      => 'required',
            'email'         => 'sometimes|required|unique:users,email,"'.base64_decode($uid).'"|max:255',
            'phoneNumber'   => 'required|unique:users,phoneNumber,"'.base64_decode($uid).'"|max:11',  
            'country'       => 'required',
            'state'         => 'required',
            'city'          => 'required',
            'address'       => 'required'  
        ]);
                
        if($validator->fails()){
            $message = $validator->messages();
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $user = User::findOrFail(base64_decode($uid));
            $user->firstName       = $inputs['firstName'];
            $user->lastName        = $inputs['lastName'];
            $user->email           = $inputs['email'];
            $user->phoneNumber     = $inputs['phoneNumber'];
            $user->country         = $inputs['country'];
            $user->state           = $inputs['state'];
            $user->city            = $inputs['city'];
            $user->address         = $inputs['address'];
            $user->status          = $inputs['status'];            
            $user->updated_at     = date('Y-m-d H:i:s');
            $user->save();
            return redirect('/clients')->with('success', 'Clients Master Data Updated Successfully');
        }
    }
    
}
