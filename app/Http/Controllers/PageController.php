<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use DataTables;
use DB;
use \Session, \Validator,\Redirect;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Page::select('id','title','content', 'image',DB::raw("DATE(created_at) created_at"));

            return Datatables::of($data)
            
           ->filterColumn('title', function($query, $keyword) {
                $query->whereRaw("title like ?", ["%{$keyword}%"]);
            })
            ->setRowId('id')
            ->addIndexColumn()            
            ->addColumn('action', function ($page) {
                return '<a title="Edit" href="'.route("page.edit",base64_encode($page->id)).'" class="btn btn-xs btn-success fa fa-edit"></a>&nbsp;<a href="javascript:void(0);" title="Delete" data-delete_url="'.route("page.destroy",base64_encode($page->id)).'" data-delete_id = "'.base64_encode($page->id).'" class="btn btn-xs btn-danger delete_data fa fa-trash"></a>';
            })            
            ->editColumn('image', function ($img) 
            {                
                $url= asset('/img/profile/'.$img->image);
                return '<img src="'.$url.'" border="0" width="100" class="img-rounded" align="center" />';                
            })
            ->editColumn('created_at', function ($p) 
            {                
                return date('d-m-Y', strtotime($p->created_at) );
            })
            ->rawColumns(['content','action','image'])->make(true);            
        }
        return view('page.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        
        $validator = Validator::make($inputs,
        [
            'title'         => 'required|max:255',
            'content'       => 'required',
            'image'         => 'sometimes|mimes:jpeg,jpg,png,gif|required|max:100000' // max 10000kb
        ]);

        if($validator->fails()){
            $message = $validator->messages();        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{

            if($request->image != ''){
                $imgName = time().".".explode('/', mime_content_type($request->image))[1];
                \Image::make($request->image)->save(public_path('img/profile/').$imgName);  
            }
            else
                $imgName = '';

            $page = new Page([
            'title'      => $inputs['title'],
            'content'    => $inputs['content'],        
            'image'      => $imgName,        
            'created_at' => date('Y-m-d H:i:s')         
            ]);
            $page->save();
            return redirect('/page')->with('success', 'Page Added Successfully');    
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageId=base64_decode($id);
        $page = Page::find($pageId);
        return view('page.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pageId = base64_decode($id);
        $inputs = $request->all();

        $validator = Validator::make($inputs,
        [
            'title'         => 'required|max:255',
            'content'       => 'required',
            'image'         => 'sometimes|mimes:jpeg,jpg,png,gif|required|max:100000' // max 10000kb
        ]);

        if($validator->fails()){
            $message = $validator->messages();        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $page = Page::findOrFail($pageId);
            if($request->image != ''){
                $imgName = time().".".$request->image->extension();
                \Image::make($request->image)->save(public_path('img/profile/').$imgName);  
            }
            else{
                if($page->image == '')
                    $imgName = '';
                else
                    $imgName = $page->image;
            }
            
            $page->title       = $inputs['title'];
            $page->content     = $inputs['content'];        
            $page->image       = $imgName;        
            $page->updated_at  = date('Y-m-d H:i:s');

            $page->save();
            return redirect('/page')->with('success', 'Page Data Updated Successfully');    
        }        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = base64_decode($id);
        $page = Page::find( $id );
        $page->delete();
        //Session::put('success', 'Course Deleted Successfully');
        return response()->json(['status'=>1,'dataId'=>$id]);        
    }
}
