<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\product;
use DataTables;
use DB;
use \Session, \Validator,\Redirect;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $productQuery = product::query();

            $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
            $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

            if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $productQuery->whereRaw("date(products.created_at ) >= '" . $start_date . "' AND date(products.created_at ) <= '" . $end_date . "'");
            }

            $data = $productQuery->select('id','pName','pDescription', 'pPrice', 'pQuantity','created_at')->orderBy('created_at','asc');

            return Datatables::of($data)
            
           ->filterColumn('pName', function($query, $keyword) {
                $query->whereRaw("pName like ?", ["%{$keyword}%"]);
            })
            ->setRowId('id')
            ->addIndexColumn()
            ->editColumn('created_at', function ($p) 
            {                
                return date('d-m-Y', strtotime($p->created_at) );
            })          
            ->addColumn('action', function ($product) {
                return '<a title="Edit" href="'.route("product.edit",base64_encode($product->id)).'" class="btn btn-xs btn-success fa fa-edit"></a>&nbsp;<a href="javascript:void(0);" title="Delete" data-delete_url="'.route("product.destroy",base64_encode($product->id)).'" data-delete_id = "'.base64_encode($product->id).'" class="btn btn-xs btn-danger delete_data fa fa-trash"></a>';
            })
            ->rawColumns(['cDescription','cThumbnail','action','cLink'])->make(true);            
            $query = $dataTable->getQuery()->get();
        }
        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('product.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $validator = Validator::make($inputs,
        [
            'pName'         => 'required|unique:products|max:255',
            'pDescription'  => 'required',
            'pPrice'        => 'required|numeric|max:1000000000',
            'pQuantity'     => 'required|numeric|max:1000000000',
            //'cFile'         => 'sometimes|mimes:jpeg,jpg,png,gif,pdf,docx,doc,xls,xlsx|max:100000'
        ]);

        if($validator->fails()){
            $message = $validator->messages();        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{

            $check_Venue_start_data = product::where('pName',$inputs['pName'])->first();

            if(count($check_Venue_start_data) == 0){

                /*if($request->cThumbnail != ''){
                    $cThumbnail = time().".".$request->cThumbnail->getClientOriginalExtension();
                    \Image::make($request->cThumbnail)->save(public_path('img/product/').$cThumbnail);  
                }
                else
                    $cThumbnail = '';*/
              
                /*if (request()->hasFile('cFile')){
                    $image = Input::file('cFile');
                    $uploadedImage = $request->file('cFile');
                    $cFile = time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/img/product/');
                    $uploadedImage->move($destinationPath, $cFile);
                    $image->imagePath = $destinationPath . $cFile;
                }            
                else
                    $cFile = '';*/

                $product = new product([
                    'pName'           => $inputs['pName'],
                    'pDescription'    => $inputs['pDescription'],
                    'pPrice'           => $inputs['pPrice'],
                    'pQuantity'      => $inputs['pQuantity'],
                    'created_at'      => date('Y-m-d H:i:s')
                ]);
                $product->save();
                return redirect('/product')->with('success', 'Product Added Successfully');
            }
            else
                return redirect('/product')->with('error', 'Another product exist.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productId=base64_decode($id);
        $product = product::find($productId);
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productId = base64_decode($id);
        $inputs = $request->all();
        $validator = Validator::make($inputs,
        [
            'pName'         => 'required|unique:products,pName,"'.$productId.'"',
            'pDescription'  => 'required',
            'pPrice'        => 'required|numeric|max:1000000000',
            'pQuantity'     => 'required|numeric|max:1000000000'
            //'cFile'         => 'sometimes|mimes:jpeg,jpg,png,gif,pdf,docx,doc,xls,xlsx|max:100000'
        ]);

        if($validator->fails()){
            $message = $validator->messages();        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{

            $check_Venue_start_data = product::where('id','!=',$productId)->where('pName',$inputs['pName'])->first();

            if(count($check_Venue_start_data) == 0){
                $product = product::findOrFail($productId);

                /*if($request->cThumbnail != ''){
                    $cThumbnail = time().".".$request->cThumbnail->getClientOriginalExtension();
                    \Image::make($request->cThumbnail)->save(public_path('img/product/').$cThumbnail);  
                }
                else{
                    if($product->cThumbnail == '')
                        $cThumbnail = '';
                    else
                        $cThumbnail = $product->cThumbnail;
                }*/
              
                /*if (request()->hasFile('cFile')){
                    $image = Input::file('cFile');
                    $uploadedImage = $request->file('cFile');
                    $cFile = time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/img/product/');
                    $uploadedImage->move($destinationPath, $cFile);
                    $image->imagePath = $destinationPath . $cFile;
                }            
                else{
                    if($product->cFile == '')
                        $cFile = '';
                    else
                        $cFile = $product->cFile;
                }*/

                $product->pName         = $inputs['pName'];
                $product->pDescription  = $inputs['pDescription'];
                $product->pPrice        = $inputs['pPrice'];
                $product->pQuantity     = $inputs['pQuantity'];                
                //$product->cFile         = $cFile;                
                $product->updated_at    = date('Y-m-d H:i:s');

                $product->save();    
                return redirect('/product')->with('success', 'product Data Updated Successfully');
            }
            else
                return redirect('/product')->with('error', 'Another product exist.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */   

    public function destroy($id)
    {
        $id = base64_decode($id);
        $product = product::find( $id );
        $product->delete();
        //Session::put('success', 'product Deleted Successfully');
        return response()->json(['status'=>1,'dataId'=>$id]);        
    }    
}
