<?php

namespace App\Http\Controllers;

use App\User;
use App\Language;
use Illuminate\Http\Request;
use DataTables;
use DB;
use \Session, \Validator,\Redirect;

class UserController extends Controller
{
    //

    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $data = User::select(DB::raw("CONCAT(firstName,' ',lastName) AS name"),'id','email', 'phoneNumber', 'address', DB::raw('(CASE
                        WHEN status = "0" THEN "Inactive" 
                        WHEN users.status = "1" THEN "Active" END) AS status'))->where('firstname','!=','admin')->where('is_user',1);

            return Datatables::of($data)
            
           ->filterColumn('name', function($query, $keyword) {
                $query->whereRaw("CONCAT(users.firstName,users.lastName) like ?", ["%{$keyword}%"]);
            })

            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                return '<a title="Edit" href="'.route("user_edit",base64_encode($user->id)).'" class="btn btn-xs btn-success fa fa-edit"></a>&nbsp;<a href="javascript:void(0);" title="Delete" data-delete_url="'.route("user_delete",base64_encode($user->id)).'" data-delete_id = "'.base64_encode($user->id).'" class="btn btn-xs btn-danger delete_data fa fa-trash"></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
            $query = $dataTable->getQuery()->get();
        }
        return view('user.index');    	
    }

    public function delete(Request $request,$id){
    	$user = User::findOrFail(base64_decode($id));
  		$user->delete();
		$request->session()->put('success', 'User Data Deleted Successfully');
		return response()->json(['status'=>1]);    	
    
    }

    public function show(){
        return view('user.add');
    }

    public function store(Request $request){ 
        $inputs = $request->all();       
        $validator = Validator::make($inputs,
        [
            'firstName'     => 'required',
            'lastName'      => 'required',
            'email'         => 'sometimes|email|max:255',
            'password'      => 'required|max:20',    
            'phoneNumber'   => 'required|max:11', 
            'country'       => 'required',
            'state'         => 'required',
            'address'       => 'required'  
        ]);
                    
        if($validator->fails()){
            $message = $validator->messages();                        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
        
            $user = new User([
                'firstName'         => $inputs['firstName'],
                'lastName'          => $inputs['lastName'],
                'email'             => $inputs['email'],
                'password'          => bcrypt($inputs['password']),
                'phoneNumber'       => $inputs['phoneNumber'],
                'country'           => $inputs['country'],
                'state'             => $inputs['state'],
                'city'              => $inputs['city'],
                'address'           => $inputs['address'],
                'status'            => $inputs['status'],                
                'created_at'        => date('Y-m-d H:i:s')             
            ]);                 
            $user->save();
            return redirect('/user')->with('success', 'User Master Data Added Successfully');
        }
    }

    public function edit($uid){
        $uid=base64_decode($uid);
        $user = User::find($uid);
        return view('user.edit', compact('user'));
    }


    public function update($uid, Request $request){ 
        $inputs = $request->all();
                
        $validator = Validator::make($inputs,
        [
            'firstName'     => 'required',
            'lastName'      => 'required',
            'email'         => 'sometimes|required|unique:users,email,"'.base64_decode($uid).'"|max:255',
            'phoneNumber'   => 'required|unique:users,phoneNumber,"'.base64_decode($uid).'"|max:11',  
            'country'       => 'required',
            'state'         => 'required',
            'city'          => 'required',
            'address'       => 'required'  
        ]);
                
        if($validator->fails()){
            $message = $validator->messages();
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
            $user = User::findOrFail(base64_decode($uid));
            $user->firstName       = $inputs['firstName'];
            $user->lastName        = $inputs['lastName'];
            $user->email           = $inputs['email'];
            $user->phoneNumber     = $inputs['phoneNumber'];
            $user->country         = $inputs['country'];
            $user->state           = $inputs['state'];
            $user->city            = $inputs['city'];
            $user->address         = $inputs['address'];
            $user->status          = $inputs['status'];            
            $user->updated_at     = date('Y-m-d H:i:s');
            $user->save();
            return redirect('/user')->with('success', 'User Master Data Updated Successfully');
        }
    }

    public function get_lat_long($address){

        $address = str_replace(" ", "+", $address);
        $json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=AIzaSyBQDPJlZeUqMN6-6F7L-cFxzo98OkzejBk");
        $json = json_decode($json);
        //dd($json);
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        return $lat.','.$long;
    }
}
