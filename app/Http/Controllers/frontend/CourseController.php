<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Course;
use App\CourseFees;
use App\CourseSchedule;
use App\TrainingSchedule;
use DB;
use Auth;
use \Session, \Validator,\Redirect;

class CourseController extends Controller
{
    //

    public function index()
    {
    	$course = Course::get();
    	return view('frontend.course.index',compact('course'));    	
    }

    public function courseList(Request $request){
    	$content = $request->content;
    	$dateSearch = $request->dateSearch;

    	$start_date = date('Y-m-d', strtotime($dateSearch));
        
    	$courseQuery = Course::query();
        $courseQuery->select('courses.id','courses.cName','courses.cDescription','courses.created_at','courses.cThumbnail');
    	$courseQuery->leftJoin('course_fees', 'course_fees.course_id', '=', 'courses.id');
    	
    	if($content != ''){
        	$courseQuery->whereRaw("courses.cName like ?", ["%{$content}%"]);
        	//$courseQuery->where('courses.cName', 'like', '%' . $content . '%');
        }

    	if($start_date != '1970-01-01'){
        $courseQuery->whereRaw("date(course_fees.start_date ) <= '" . $start_date . "' AND date(course_fees.end_date ) >= '" . $start_date . "'");
        }
           	
        $data =  $courseQuery->get();
        //$html = $courseQuery->toSql();

        $html = view('frontend.course.ajax-course',compact('data'))->render();
    		
    	if($html != '')
    		return response()->json(['msg' => 'success','structure' => $html]);
    	else
    		return response()->json(['msg' => 'success','structure' => 'No Course Found']);
    	
    }

    public function courseDetails($id){

    	$id = base64_decode($id);
    	$details = Course::where('id',$id)->with('schedule','fees')->get();
    	
        /*echo "<pre>";
        print_r($details);exit;*/
        return view('frontend.course.course-details',compact('details'))->render();
    }

    public function courseTrainingDetails($id){
    	$id = base64_decode($id);
    	$course = Course::where('id',$id)->with('schedule')->get();

    	return view('frontend.course.course-training-details',compact('course'))->render();    	
    }

    public function schedule(){
    	$courses = TrainingSchedule::select('courses.id','courses.cName','training_schedule.availableSeat','training_schedule.start_date','training_schedule.start_time')
		->join('courses', 'training_schedule.course_id', '=', 'courses.id')		
		->get()->all();
    	return view('frontend.course.course-schedule-details',compact('courses'))->render();    	
    }

    public function checkoutDetails($courseId,$courseScheduleId){
        $courseId = base64_decode($courseId);
        $courseScheduleId = base64_decode($courseScheduleId);

        $data = Course::with(['fees','schedule' => function($q) use ($courseScheduleId){
            $q->where('id', '=', $courseScheduleId);
        }])->where('id',$courseId)->get();

        return view('frontend.course.course-checkout',compact('data','courseScheduleId'))->render();            
    }

    public function coursePaymentFees(Request $request){

        $paymentMethod = $request->paymentMethod;
        $courseFees = $request->courseFees;
        $courseId = $request->courseId;
        $training_schedule_id = $request->courseScheduleId;
        $userId = Auth::user()->id;

        $checkExist = CourseSchedule::where('course_id',$courseId)->where('training_schedule_id',$training_schedule_id)->where('user_id',$userId)->get();
        if($checkExist->count() >= 1)
        {
            return \Redirect::route('user.user-course', base64_encode($userId))->with('error', 'Course already subscribed');
        }
        else
        {
            $CourseSchedule = new CourseSchedule([
                'course_id'             => $courseId,
                'user_id'               => $userId,
                'training_schedule_id'  => $training_schedule_id,
                'pay'                   => $courseFees,
                'created_at'            => date('Y-m-d H:i:s')
            ]);
            $CourseSchedule->save();

            /*  CALCULATE NUMBER OF SEAT AAVAILABLE ........*/
            if($CourseSchedule->id >= 1){

                $getSeatCount = TrainingSchedule::select('seatCapacity')->where('course_id',$courseId)->where('id',$training_schedule_id)->first();
                $getReserveSeat = CourseSchedule::where('course_id',$courseId)->where('training_schedule_id',$training_schedule_id)->get();

                $getSeatAvailable = $getSeatCount->seatCapacity - $getReserveSeat->count();


                $TrainingSchedule = TrainingSchedule::findOrFail($training_schedule_id);
                $TrainingSchedule->availableSeat  = $getSeatAvailable;
                $TrainingSchedule->save();        
            }
            /*  CALCULATE NUMBER OF SEAT AAVAILABLE ........*/

            return \Redirect::route('user.user-course', base64_encode($userId))->with('success', 'Course subscribed successfully');
        }
    }
}
