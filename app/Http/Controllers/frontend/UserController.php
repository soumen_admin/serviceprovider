<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use App\User;
use App\CourseSchedule;
use App\Language;
use DB;
use \Session, \Validator,\Redirect;

class UserController extends Controller
{
    //

    public function profile($id){
    	$id = base64_decode($id);
    	$userData = User::find($id);

    	return view("frontend.user.profile",compact('userData'));
    }

    public function userCourse($id){

    	$id = base64_decode($id);
    	$userCourse = CourseSchedule::with('course','user','training')->where('user_id',$id)->get();
        return view("frontend.user.course",compact('userCourse'));
    }
	
	public function changepassword($id){
		$id = base64_decode($id);
		$userData = User::find($id);
		return view("frontend.user.changepassword",compact('userData'));
	}

    public function change(Request $request){
	    $id = $request->get('user_id');
		$userData = User::find($id);
		
		$inputs = $request->all();

       if (!(Hash::check($request->get('current-password'),$userData->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Current Password cannot be same as your new password. Please choose a different password.");
        }
		
		if($request->get('new-password')!= $request->get('new-password_confirmation')){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password and Confirm Password not same.");
        }


        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        //Change Password

		$userData->password        = bcrypt($request->get('new-password'));
      
        $userData->save();

        return redirect()->back()->with("success","Password changed successfully !");
    }	
}
