<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{

	public function sendMail(Request $request){

		$data = array('name'=>"Virat Gandhi"); // create an array with response..

	      Mail::send('frontend.mail.index', $data, function($message) {
	         $message->to('abc@gmail.com', 'ROI')->subject
	            ('Contact Us');
	         $message->from('xyz@gmail.com','Virat Gandhi');
	      });
	      
	      return response()->json(['status'=>1,'msg'=>'Mail send successfully']);
	}
    
}
