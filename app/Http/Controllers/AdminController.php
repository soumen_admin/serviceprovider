<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use \Session, \Validator,\Redirect;

class AdminController extends Controller
{
    //

	public function index(Request $request){
		
		//echo "<pre>"; print_r($request->all());	exit;

		$inputs = $request->all();
        $validator = Validator::make($inputs,
        [
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',

        ]);
        if($validator->fails()){
            $message = $validator->messages();        
            return Redirect::back()->withErrors($validator)->withInput();
        }
        else{
        	$password = $request->password;

			$user = User::findOrFail(auth()->user()->id);
			$user->password = Hash::make($password);
	        $user->setRememberToken(Str::random(60));
	        $user->save();
	        return redirect('/user')->with('success', 'Password Updated Successfully');
        }	
	}

	public function profile(){
		return view('admin.profile');
	}
}
