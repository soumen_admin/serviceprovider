<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseFees extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'course_fees';
    protected $fillable = ['course_id', 'cVenue', 'cPrice','start_date','end_date','deleted_at'];


	public function course(){
		return $this->hasOne('App\Course');
	}
	
}
