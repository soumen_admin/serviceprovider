<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseSchedule extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'courseschedule';
    protected $fillable = ['course_id', 'user_id', 'pay','training_schedule_id','deleted_at'];


	public function course(){
		return $this->hasOne('App\Course','id','course_id');
	}

	public function user(){
		return $this->hasOne('App\User','id','user_id');
	}

	public function training(){
		return $this->hasOne('App\TrainingSchedule','id','training_schedule_id');
	}
}
