<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingSchedule extends Model
{
    protected $dates = ['deleted_at'];
    protected $table = 'training_schedule';
    protected $fillable = ['course_id', 'seatCapacity', 'availableSeat','start_date','start_time'];


	public function course(){
		return $this->hasOne('App\Course');
	}
}
