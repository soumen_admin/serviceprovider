<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'courses';
    protected $fillable = ['cName', 'cDescription', 'cLink','cThumbnail','cFile','deleted_at'];


    public function schedule(){
    	return $this->hasMany('App\TrainingSchedule');
    }

    public function fees(){
    	return $this->hasMany('App\CourseFees');
    }

}
