-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2019 at 03:54 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mazad`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_parent` tinyint(4) NOT NULL DEFAULT 1,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categoryName_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categoryDesc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categoryDesc_ar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categoryImg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1.Active,0.Inactive',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `is_parent`, `categoryName`, `categoryName_ar`, `categoryDesc`, `categoryDesc_ar`, `categoryImg`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'Tickets22', NULL, 'Suggestion Tickets', NULL, 'test.jpg', 1, '2019-09-20 05:45:30', NULL, '2019-09-20 05:45:30'),
(2, 0, 'Pet Supplies', 'مستلزمات الحيوانات الأليفة', 'Pet Supplies description', 'مستلزمات الحيوانات الأليفةمستلزمات الحيوانات الأليفةمستلزمات الحيوانات الأليفة', '1569565587.jpg', 1, NULL, '2019-09-20 05:08:58', '2019-09-27 00:56:27'),
(3, 0, 'Business Equipment', 'معدات العمل', 'Business Equipment', 'معدات العملمعدات العملمعدات العملمعدات العملمعدات العمل', '1568984122.png', 1, NULL, '2019-09-20 06:00:07', '2019-09-23 07:31:04'),
(4, 0, 'Campers', 'المعسكر', 'Campers', 'المعسكرالمعسكرالمعسكرالمعسكرالمعسكرالمعسكرالمعسكر', '1569223148.jpg', 1, NULL, '2019-09-20 06:08:46', '2019-09-23 01:49:08'),
(5, 0, 'Weddding', 'حفل زواج', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 'لصناعة الطباعة والتنضيد. كان Lorem Ipsum هو النص الوهمي القياسي في هذه الصناعة منذ القرن الخامس عشر الميلادي ، عندما أخذت طابعة غير معروفة لوحًا من نوعه وتدافعت عليه لصنع كتاب نموذج للعينات. لقد نجا ليس فقط خمسة قرون ، ولكن أيضا قفزة في التنضيد الإلكتروني ، تبقى دون تغيير أساسي. تم الترويج لها في الستينيات من القرن الماضي من خلال إصدار أوراق Letraset التي تحتوي على مقاطع Lorem Ipsum ، ومؤخراً مع برنامج النشر المكتبي مثل Aldus PageMaker بما في ذلك إ', '1568985393.jpg', 1, NULL, '2019-09-20 07:46:33', '2019-09-23 01:48:31'),
(6, 1, 'testing PetSupplies', NULL, 'testing PetSupplies', NULL, '1569225358.jpg', 1, NULL, '2019-09-23 02:25:58', '2019-09-23 02:25:58'),
(7, 1, NULL, 'زفاف الفرعية', NULL, 'زفاف الفرعيةزفاف الفرعية', '1569243972.jpg', 1, NULL, '2019-09-23 07:36:12', '2019-09-23 07:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lang_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1.Active,0.Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `lang_name`, `code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 1, '2019-09-10 18:30:00', '2019-09-10 18:30:00'),
(2, 'Arabic', 'ar', 1, '2019-09-11 18:30:00', '2019-09-11 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_11_094135_add_language_table', 2),
(4, '2019_09_11_101505_update_language_table', 3),
(6, '2019_09_11_101942_update_users_table', 4),
(7, '2019_09_20_075029_create_ad_categories', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstName_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1234',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double(8,2) DEFAULT NULL,
  `longitude` double(8,2) DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1.Active,0.Inactive',
  `is_verified` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1.Yes,0.No',
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `firstName_ar`, `lastName`, `lastName_ar`, `email`, `password`, `phoneNumber`, `otp`, `image`, `latitude`, `longitude`, `country`, `country_ar`, `state`, `state_ar`, `city`, `city_ar`, `address`, `address_ar`, `status`, `is_verified`, `remember_token`, `access_token`, `created_at`, `updated_at`) VALUES
(1, 'Gabe', NULL, 'Christiansen', NULL, 'dskiles@gmail.com', '{Z8$nES2.Br0SOHIOvQ', '123456789', '1234', '', -56.03, -88.91, 'Guam', NULL, 'North Dakota', NULL, 'Wilhelmineland', NULL, '43268 Barton Green Apt. 925\r\nJerdeshire, MO 33248', NULL, 1, 1, NULL, '', '2019-09-06 04:53:58', '2019-09-20 01:58:50'),
(2, 'Daren', NULL, 'Conn', NULL, 'isabelle.block@greenfelder.org', '.;((_sLf', '1234567890', '1234', '', 11.28, 70.82, 'Saint Vincent and the Grenadines', NULL, 'New Mexico', NULL, 'Gradychester', NULL, '74401 Nienow Extensions Apt. 276\r\nLouchester, MT 13328-5026', NULL, 1, 1, NULL, '', '2019-09-06 04:53:59', '2019-09-20 01:36:26'),
(3, 'Marques', NULL, 'Ziemann', NULL, 'larson.cathy@hotmail.com', 'Wty^\'yB$:y*GUg', '+91-975863945', '1234', '', 55.06, -125.20, 'Haiti', NULL, 'Connecticut', NULL, 'East Beaulah', NULL, '2926 Carter Port\r\nLittelbury, NY 54160-0068', NULL, 1, 1, NULL, '', '2019-09-06 04:54:01', '2019-09-11 03:48:09'),
(4, 'Richie', NULL, 'Larkin', NULL, 'wilbert.bernhard@dooley.com', '!MN(|<#9{b', '(228) 370-1562 x36506', '1234', 'ead3f2723541cdb5533af1c7ee325ba6.jpg', 73.18, -139.53, 'Myanmar', NULL, 'Washington', NULL, 'West Cathy', NULL, '66321 Marvin Hill\nLake Randyhaven, ND 45178', NULL, 1, 0, NULL, '', '2019-09-06 04:54:02', '2019-09-06 04:54:02'),
(5, 'Velva', NULL, 'Stoltenberg', NULL, 'nwillms@hotmail.com', 'gliclqbD3p,@', '1-743-630-2883 x1289', '1234', '1082929293aae5eda365b24a14124995.jpg', 34.76, -101.71, 'Ecuador', NULL, 'Ohio', NULL, 'Brakusville', NULL, '947 Stanton Highway\nSouth Trystanberg, MN 80240', NULL, 1, 0, NULL, '', '2019-09-06 04:54:03', '2019-09-06 04:54:03'),
(6, 'Peter', NULL, 'Metz', NULL, 'ericka.rosenbaum@yahoo.com', '1`Z;XmBf/#]J;my>', '(440) 855-0913 x110', '1234', '6cd431e017d7c2f28c5231ad05ae29a4.jpg', 46.25, -43.57, 'Honduras', NULL, 'Delaware', NULL, 'Stoltenbergport', NULL, '12720 Shanahan Cove\nNorth Kristoferside, UT 26250-0820', NULL, 1, 0, NULL, '', '2019-09-06 04:54:04', '2019-09-06 04:54:04'),
(7, 'Dan', NULL, 'Wintheiser', NULL, 'estella.gleason@smitham.com', '<iX4\"&8l%wBR', '+16536947944', '1234', '0dc58b8b6c9c0976fa76c925462f99cd.jpg', -60.46, 0.90, 'Estonia', NULL, 'Texas', NULL, 'South Mckenzieton', NULL, '13160 Courtney Fords\nSouth Nannie, AZ 81737-8349', NULL, 1, 0, NULL, '', '2019-09-06 04:54:05', '2019-09-06 04:54:05'),
(8, 'Danika', NULL, 'Wilderman', NULL, 'maeve43@shanahan.com', '4bj_3=s)(', '908.670.8565', '1234', 'd210d0f7f4c036623976fdec309d1128.jpg', 56.28, -136.25, 'Georgia', NULL, 'Arizona', NULL, 'Adantown', NULL, '169 Lou Summit Suite 946\nArtmouth, MA 14156-5713', NULL, 1, 0, NULL, '', '2019-09-06 04:54:06', '2019-09-06 04:54:06'),
(9, 'Jessy', NULL, 'Kozey', NULL, 'dprohaska@gmail.com', 'EW;[QH', '721.376.2849 x47634', '1234', '0ea5dfb8c34116c775ea18e38ce63f33.jpg', -46.35, -162.67, 'Cuba', NULL, 'California', NULL, 'Port Chesleyfurt', NULL, '9513 Langworth Keys\nNorth Stephanberg, ND 00503-3079', NULL, 1, 0, NULL, '', '2019-09-06 04:54:07', '2019-09-06 04:54:07'),
(11, 'Karine', NULL, 'Sanford', NULL, 'langworth.brendan@block.org', 'dXw6L@.2s%;', '(516) 307-4569', '1234', '74ce67532fab70c53bf33209f8476c51.jpg', -72.59, 62.29, 'Solomon Islands', NULL, 'Rhode Island', NULL, 'New Garettmouth', NULL, '402 Lolita Crescent\nNorth Luciomouth, CO 84081-3494', NULL, 1, 0, NULL, '', '2019-09-06 04:54:09', '2019-09-06 04:54:09'),
(12, 'Enid', NULL, 'Murphy', NULL, 'bmills@stroman.com', 'Dp\"fZq#\"', '(248) 858-6028 x114', '1234', 'aaa2c80a3f0071bb82c4aefe7a0ccf1f.jpg', 45.65, -12.01, 'Netherlands Antilles', NULL, 'Illinois', NULL, 'Stoneland', NULL, '1910 Armstrong Tunnel Apt. 178\nPort Reedhaven, GA 90124-9187', NULL, 1, 0, NULL, '', '2019-09-06 04:54:11', '2019-09-06 04:54:11'),
(13, 'Dwight', NULL, 'Durgan', NULL, 'funk.delta@hyatt.net', 'BB[!~I?PbL', '251.213.7510 x66751', '1234', '00a9d40868ebfa5524b96021e6a03686.jpg', -63.97, 92.92, 'Thailand', NULL, 'Maine', NULL, 'Margaretmouth', NULL, '88444 Rhoda Dam Suite 331\nKurtisbury, MN 88627-4303', NULL, 1, 0, NULL, '', '2019-09-06 04:54:12', '2019-09-06 04:54:12'),
(14, 'Leann', NULL, 'Greenfelder', NULL, 'barrows.hillary@haag.com', 'xGrQPR44MA2KQa@_', '1-992-728-0212', '1234', '7d97833e9dcacb1e0aed46c9da623e4a.jpg', 15.46, -60.01, 'Sierra Leone', NULL, 'Florida', NULL, 'Lake Hoyt', NULL, '5431 Purdy Shoals\nSouth Hiram, MD 38013-9814', NULL, 1, 0, NULL, '', '2019-09-06 04:54:13', '2019-09-06 04:54:13'),
(15, 'Brody', NULL, 'Beer', NULL, 'lonzo81@hills.com', '-7#ak?VIRcJ', '528.414.3922 x7640', '1234', '0684b859506204f6b5bc925ba565ee4a.jpg', -51.36, -99.75, 'Timor-Leste', NULL, 'Montana', NULL, 'Handport', NULL, '47356 Torrance Mission\nBeahanshire, IL 10445-1134', NULL, 1, 0, NULL, '', '2019-09-06 04:54:14', '2019-09-06 04:54:14'),
(16, 'Kiel', NULL, 'Frami', NULL, 'damion.haag@gmail.com', 'Yz\'bcuY<>Nh<AO\"^', '(395) 555-4787 x1919', '1234', '60ed77826b0b10a52e941fb7c16a550a.jpg', -65.68, 90.80, 'Ecuador', NULL, 'North Dakota', NULL, 'New Remington', NULL, '76699 Juliana Glen\nWest Brandt, NH 02658-8912', NULL, 1, 0, NULL, '', '2019-09-06 04:54:16', '2019-09-06 04:54:16'),
(17, 'Gust', NULL, 'Reinger', NULL, 'dare.orval@gmail.com', 'D(\"n_#}#^~+itQ8\'0;', '994.933.6185', '1234', '59355ab0de8f73fe83bd6d130bd9f980.jpg', 39.56, 143.05, 'Macedonia', NULL, 'North Dakota', NULL, 'East Michel', NULL, '6471 Frances Mall Apt. 158\nNorth Romahaven, OK 87159-4427', NULL, 1, 0, NULL, '', '2019-09-06 04:54:17', '2019-09-06 04:54:17'),
(18, 'Angelica', NULL, 'Turcotte', NULL, 'mandy10@ledner.com', '`Mx,1?*dfI3ZOs3', '357.255.8024 x823', '1234', '16c06b332de7474226a73e1033193260.jpg', -17.26, 25.72, 'Libyan Arab Jamahiriya', NULL, 'Louisiana', NULL, 'Coraliestad', NULL, '19830 Marcella Rapids Suite 613\nWest Rhodaside, AK 96119', NULL, 1, 0, NULL, '', '2019-09-06 04:54:18', '2019-09-06 04:54:18'),
(19, 'Lera', NULL, 'Bogisich', NULL, 'shad.heidenreich@ondricka.com', 'D9S(LZN>;o^', '+1 (470) 672-8203', '1234', '21c29828346c07e914fd7fd435ea4d75.jpg', -11.47, -172.43, 'Antarctica (the territory South of 60 deg S)', NULL, 'West Virginia', NULL, 'South Schuylerchester', NULL, '760 Walsh Walks Apt. 803\nBrentmouth, NH 79780-4998', NULL, 1, 0, NULL, '', '2019-09-06 04:54:19', '2019-09-06 04:54:19'),
(20, 'Catharine', NULL, 'Romaguera', NULL, 'margaretta.zemlak@yahoo.com', 'mNMWtCrw|U0X7gs@', '+12173873605', '1234', '3c4054c476dc41dbb67a78d1dca4f14c.jpg', 34.68, 103.82, 'Nepal', NULL, 'Ohio', NULL, 'Jasonport', NULL, '925 Kuphal Ports\nNorth Tate, MS 92664-4144', NULL, 1, 0, NULL, '', '2019-09-06 04:54:20', '2019-09-06 04:54:20'),
(21, 'Alfonso', NULL, 'Langosh', NULL, 'trenton23@keeling.com', 'm_Od2Olx:', '(225) 323-2206 x66037', '1234', '098a50e98ed7ae7cd1b171648af305d5.jpg', 38.26, 129.36, 'Uzbekistan', NULL, 'South Dakota', NULL, 'Ryannstad', NULL, '932 Josiane Expressway\nEast Immanuel, TN 44228', NULL, 1, 0, NULL, '', '2019-09-06 04:54:21', '2019-09-06 04:54:21'),
(22, 'Cruz', NULL, 'Dare', NULL, 'yterry@grady.com', 'sba!\'[is;7a*', '(639) 933-2660 x28560', '1234', 'a3ea5d79c995f39b5e31ece453732775.jpg', -9.28, 85.36, 'British Virgin Islands', NULL, 'Massachusetts', NULL, 'Hahntown', NULL, '57453 Pfannerstill Meadow Apt. 303\nLittelbury, RI 23391', NULL, 1, 0, NULL, '', '2019-09-06 04:54:22', '2019-09-06 04:54:22'),
(23, 'Reece', NULL, 'Gerlach', NULL, 'qolson@gmail.com', 'mW[zR]=', '1-860-324-0465 x521', '1234', '70e43ae027be727a925d573d34cd1f2f.jpg', -86.68, -163.75, 'United States Virgin Islands', NULL, 'Ohio', NULL, 'Josefinaside', NULL, '540 Rohan Island Apt. 306\nNorth Moriahbury, WA 00516-5107', NULL, 1, 0, NULL, '', '2019-09-06 04:54:23', '2019-09-06 04:54:23'),
(24, 'Tierra', NULL, 'Sawayn', NULL, 'muhammad94@hotmail.com', 'HCe%K!\"5zV;Ge*&[7', '739.481.3702', '1234', '9b6b59182b89627236791c1eb369bf1d.jpg', -50.57, -17.04, 'Andorra', NULL, 'Illinois', NULL, 'Legrosview', NULL, '4579 Stokes Hills\nWest Chayaport, MI 31940', NULL, 1, 0, NULL, '', '2019-09-06 04:54:24', '2019-09-06 04:54:24'),
(25, 'Vella', NULL, 'Little', NULL, 'frami.kaley@yahoo.com', 'N~Jl1gQ]daGmVcy<rg', '+1 (610) 588-8845', '1234', '2bc00789a2c97d14f75493c94472db1e.jpg', 57.04, 31.75, 'Tunisia', NULL, 'Georgia', NULL, 'North Dwight', NULL, '27619 Kameron Extensions Apt. 194\nLake Candaceberg, IA 16265-5126', NULL, 1, 0, NULL, '', '2019-09-06 04:54:25', '2019-09-06 04:54:25'),
(26, 'Lavon', NULL, 'Keeling', NULL, 'katherine09@yahoo.com', '*bLgY&@ji\'3dLD/dX', '1-235-506-1563', '1234', 'c7352aff221ad8d280bc41c77d6a1148.jpg', -12.75, 161.99, 'Hong Kong', NULL, 'Montana', NULL, 'East Giuseppeshire', NULL, '60537 Johnpaul Light\nPort Ambroseville, OH 27501-7928', NULL, 1, 0, NULL, '', '2019-09-06 04:54:26', '2019-09-06 04:54:26'),
(27, 'Madie', NULL, 'Morissette', NULL, 'trolfson@hotmail.com', 'btdvo~SS!', '+1.237.253.9583', '1234', '552f2018388e269f3c483db18b14253e.jpg', -46.15, -63.53, 'Mongolia', NULL, 'Georgia', NULL, 'Margaretebury', NULL, '99754 Pouros Rest\nWest Terrencefort, RI 89196-5420', NULL, 1, 0, NULL, '', '2019-09-06 04:54:27', '2019-09-06 04:54:27'),
(28, 'Enola', NULL, 'Gusikowski', NULL, 'chance.emmerich@weber.com', '/S%`ey_EzG', '+13074279672', '1234', '818acae5c711cf2ddc694a848fab362a.jpg', 32.07, 162.54, 'South Africa', NULL, 'Florida', NULL, 'Port Rhettville', NULL, '100 Abshire Circles\nFeestbury, VT 04143-2369', NULL, 1, 0, NULL, '', '2019-09-06 04:54:28', '2019-09-06 04:54:28'),
(29, 'Khalid', NULL, 'Maggio', NULL, 'erutherford@gmail.com', ':ck?;?]a\\H\'&', '423.647.3583', '1234', '553ae3ae45f06d4796f32f45a9b9d003.jpg', -23.61, -39.22, 'Micronesia', NULL, 'Pennsylvania', NULL, 'North Crawford', NULL, '161 Glover Ports Suite 426\nBoyermouth, RI 26032-2454', NULL, 1, 0, NULL, '', '2019-09-06 04:54:29', '2019-09-06 04:54:29'),
(30, 'Viola', NULL, 'Lind', NULL, 'jerry.dare@gmail.com', 'dc:2mg)if6', '770-266-0660 x975', '1234', '680dffcde8b885495f0e264528368646.jpg', -62.67, -9.57, 'Heard Island and McDonald Islands', NULL, 'Georgia', NULL, 'North Remingtonberg', NULL, '60580 Luisa Keys\nNew Cindyburgh, WV 95090', NULL, 1, 0, NULL, '', '2019-09-06 04:54:30', '2019-09-06 04:54:30'),
(31, 'Emilio', NULL, 'Wisoky', NULL, 'swaniawski.bart@wiza.com', '$2G:pb9?!71K', '1-886-697-0432 x0169', '1234', '99a58f96afc84c0b87ae831c6a740f23.jpg', -8.23, 131.61, 'Guinea-Bissau', NULL, 'Montana', NULL, 'East Laverneburgh', NULL, '55875 Ankunding Knolls Apt. 041\nEast Zellafort, TX 64338', NULL, 1, 0, NULL, '', '2019-09-06 04:54:31', '2019-09-06 04:54:31'),
(32, 'Jayde', NULL, 'Toy', NULL, 'coralie66@hammes.com', '@BP*:!xbxdCTfRa', '1-804-659-5891 x6427', '1234', 'c99ab28438c43688209c949a16ca282d.jpg', 48.62, -155.04, 'Macedonia', NULL, 'Maine', NULL, 'Lake Zella', NULL, '7400 Whitney Courts Suite 415\nNew Morganhaven, CA 03768-4194', NULL, 1, 0, NULL, '', '2019-09-06 04:54:32', '2019-09-06 04:54:32'),
(33, 'Sabryna', NULL, 'Spencer', NULL, 'charlene.pfannerstill@gmail.com', 'vpoUD~_Su/kInp+', '303.450.7836', '1234', '036494179e685dbd147a18d423f6040a.jpg', 27.83, -151.37, 'Haiti', NULL, 'Tennessee', NULL, 'Gusmouth', NULL, '630 Halvorson Locks Apt. 926\nLabadiechester, IN 26125', NULL, 1, 0, NULL, '', '2019-09-06 04:54:33', '2019-09-06 04:54:33'),
(34, 'Loma', NULL, 'Hodkiewicz', NULL, 'aleen12@medhurst.com', 'G61HK69lEW;', '867-897-4988 x7050', '1234', '35a40dd52f807c8ae07f01db803caf26.jpg', 49.04, 75.86, 'Niue', NULL, 'West Virginia', NULL, 'Lake Torreytown', NULL, '36396 Tanya Road\nLake Joy, KS 72758-5585', NULL, 1, 0, NULL, '', '2019-09-06 04:54:34', '2019-09-06 04:54:34'),
(35, 'Lee', NULL, 'Gleichner', NULL, 'zaria18@farrell.com', 'Q$wdypF\"', '840-690-2928', '1234', '6a1ad64346473ffe8d5f9599e370dd4d.jpg', -79.30, -80.32, 'Brunei Darussalam', NULL, 'New Mexico', NULL, 'Estherberg', NULL, '1143 Schuster Inlet\nEast Santina, SC 27372-5769', NULL, 1, 0, NULL, '', '2019-09-06 04:54:35', '2019-09-06 04:54:35'),
(36, 'Anahi', NULL, 'Kunze', NULL, 'yhudson@kutch.com', 'Q*di(^J^V#>\"Q3', '941-705-4553 x334', '1234', 'cffe4918f32b712f4c76dc4f0b289378.jpg', -7.27, 27.86, 'Anguilla', NULL, 'Wyoming', NULL, 'West Dan', NULL, '101 Cronin Heights Suite 200\nLake Ashlee, WV 01257', NULL, 1, 0, NULL, '', '2019-09-06 04:54:36', '2019-09-06 04:54:36'),
(37, 'Cordia', NULL, 'Lockman', NULL, 'hhomenick@hotmail.com', '!.){/{[>{Z', '758-300-3337', '1234', '449c27f1727d529197ebbb6df38048a6.jpg', 50.60, 102.01, 'Suriname', NULL, 'Alaska', NULL, 'Port Mazie', NULL, '4405 Hester Alley\nQueeniefort, MT 34341-4160', NULL, 1, 0, NULL, '', '2019-09-06 04:54:37', '2019-09-06 04:54:37'),
(38, 'Buddy', NULL, 'Hansen', NULL, 'woodrow51@schiller.org', '7;}md}KS{jfmtkig1xt', '(620) 737-2809 x7579', '1234', '426505cf1b273e153907bfbb38ee9bb4.jpg', -25.84, 138.39, 'Burkina Faso', NULL, 'South Dakota', NULL, 'Bayerburgh', NULL, '408 Milo Gateway\nCarrollshire, LA 31780-9149', NULL, 1, 0, NULL, '', '2019-09-06 04:54:38', '2019-09-06 04:54:38'),
(39, 'Rosanna', NULL, 'Grady', NULL, 'metz.jacinto@hodkiewicz.com', '>2&t=Z?MTzh', '1-580-421-4557 x81231', '1234', 'a034fdcf7cb9800ebd9e97101402187d.jpg', 84.95, 60.39, 'Paraguay', NULL, 'Wisconsin', NULL, 'Lake Willy', NULL, '1147 Anita Stream Apt. 873\nWest Marisolhaven, LA 44673', NULL, 1, 0, NULL, '', '2019-09-06 04:54:39', '2019-09-06 04:54:39'),
(40, 'Twila', NULL, 'Towne', NULL, 'elna.bradtke@gmail.com', 'FPc5,!', '(393) 518-0620 x83664', '1234', 'e9a4627261cf5ace31e08613c15b0e43.jpg', 31.36, -74.48, 'Tokelau', NULL, 'Nebraska', NULL, 'Kennaton', NULL, '572 Torphy Mission\nConnellybury, SD 55996-6558', NULL, 1, 0, NULL, '', '2019-09-06 04:54:40', '2019-09-06 04:54:40'),
(41, 'Cassandra', NULL, 'Ortiz', NULL, 'hildegard.lebsack@smith.com', '1=7gZ8QD', '707.305.0012 x75204', '1234', '50ac89b4e68ec2604d9d9be31f4cf62b.jpg', -19.04, 158.40, 'Togo', NULL, 'Alaska', NULL, 'East Regan', NULL, '7172 Halle Hill\nKennafort, GA 75689-2900', NULL, 1, 0, NULL, '', '2019-09-06 04:54:41', '2019-09-06 04:54:41'),
(42, 'Garrison', NULL, 'Brekke', NULL, 'parker.waelchi@yahoo.com', 'y{G4Btpj0G:LN?]r', '(474) 306-8955 x28695', '1234', '5393872e0ed354a4a194b42920f1fe6b.jpg', -61.88, 70.83, 'Liberia', NULL, 'Georgia', NULL, 'West Dorothyshire', NULL, '4723 Gutmann Drives Suite 715\nNorth Dianaport, GA 54857', NULL, 1, 0, NULL, '', '2019-09-06 04:54:42', '2019-09-06 04:54:42'),
(43, 'Jewell', NULL, 'Senger', NULL, 'monahan.mandy@hotmail.com', '=4%SP!A/7iu', '+1-889-281-2641', '1234', 'a7a959242da0192bf3fa0df088beabf4.jpg', 13.47, 115.17, 'Micronesia', NULL, 'Georgia', NULL, 'New Linnie', NULL, '88946 Jerod Groves Apt. 771\nWest Melanyshire, PA 40436', NULL, 1, 0, NULL, '', '2019-09-06 04:54:44', '2019-09-06 04:54:44'),
(44, 'Vesta', NULL, 'Wyman', NULL, 'marguerite.sauer@hotmail.com', '}x.e#J~*iPa~<ST?h42`', '(757) 995-6825', '1234', '47917aff49500dac6cb1a9fd82afff39.jpg', 77.30, -155.25, 'Northern Mariana Islands', NULL, 'New Hampshire', NULL, 'Murraymouth', NULL, '9405 Kulas Creek\nNorth Angela, KY 52608-2286', NULL, 1, 0, NULL, '', '2019-09-06 04:54:44', '2019-09-06 04:54:44'),
(45, 'Ezra', NULL, 'Hammes', NULL, 'elza.hayes@hotmail.com', 'zDh,l*yOI', '+19794460773', '1234', 'b306af08fef7d191943d32758c56fef0.jpg', -0.20, -97.50, 'Saint Lucia', NULL, 'Maine', NULL, 'Stammville', NULL, '18299 Brekke Mall\nNorth Grayceville, TN 66044', NULL, 1, 0, NULL, '', '2019-09-06 04:54:45', '2019-09-06 04:54:45'),
(46, 'Dexter', NULL, 'Reynolds', NULL, 'pkoepp@gmail.com', ']ew,F+re.XWlKNPi&}g;', '(782) 445-8400 x587', '1234', '985d65830323933f5495b04a4ca85979.jpg', 83.18, 151.63, 'Zambia', NULL, 'Tennessee', NULL, 'Dallinmouth', NULL, '39572 Gerlach Station Suite 076\nEast Krystina, KS 25080-4598', NULL, 1, 0, NULL, '', '2019-09-06 04:54:47', '2019-09-06 04:54:47'),
(47, 'Angeline', NULL, 'Gerhold', NULL, 'humberto47@gmail.com', '7f9w)V7=', '+1-741-714-1340', '1234', '252fe57b9abc136919b96b90da9d534d.jpg', -56.47, -109.38, 'Falkland Islands (Malvinas)', NULL, 'Massachusetts', NULL, 'Spencermouth', NULL, '87467 Dangelo Lane\nSouth Sibyl, MS 16838', NULL, 1, 0, NULL, '', '2019-09-06 04:54:48', '2019-09-06 04:54:48'),
(48, 'Ozella', NULL, 'Keeling', NULL, 'iva.kautzer@hotmail.com', 'avio8\'GgiYJo6kJ\\w_', '809.520.8748 x762', '1234', 'cf9145ed6359c690e3d4c4809aaa283e.jpg', 67.64, 160.25, 'Papua New Guinea', NULL, 'Connecticut', NULL, 'Brennahaven', NULL, '1300 Tevin Orchard\nChristiansenhaven, SC 77596-7241', NULL, 1, 0, NULL, '', '2019-09-06 04:54:49', '2019-09-06 04:54:49'),
(49, 'Bud', NULL, 'Koch', NULL, 'ushanahan@haley.com', 'HS~?[y2{N|qn6', '(620) 380-0666', '1234', 'd3af42a1f97f736610fe0afcc0345751.jpg', 47.07, -153.48, 'Serbia', NULL, 'Illinois', NULL, 'Ocieton', NULL, '392 Delia Circle Apt. 861\nLeathahaven, TX 65386', NULL, 1, 0, NULL, '', '2019-09-06 04:54:50', '2019-09-06 04:54:50'),
(50, 'Desmond', NULL, 'Gibson', NULL, 'lwindler@schulist.biz', '_K<Y`V|}zhB', '+1.628.398.6409', '1234', 'cc6f6047ac6bae755cfe44e701056fca.jpg', 63.98, -52.89, 'Korea', NULL, 'Connecticut', NULL, 'Beahanfurt', NULL, '55294 Friesen Isle\nBednarville, OK 20656', NULL, 1, 0, NULL, '', '2019-09-06 04:54:51', '2019-09-06 04:54:51'),
(51, 'Ravi', NULL, NULL, NULL, 'ravi.kumar@cogitosoftware.in', '$2y$10$wQ0sfoZbaH1FO.e7MzLwb.JdNHMcOrE0KmoZjW7cmqEzPScDMhWCO', NULL, '1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'QGdqCFhB0YgccTEEfZEl2ngJA0YyVRE9BFLTTd0jfPuIrQMj0rT0SA1MeyoA', '', '2019-09-06 04:57:10', '2019-09-06 04:57:10'),
(56, 'Pintu', 'نصف لتر', 'Naskar', 'ناس', 'pintu.naskar@cogitosoftware.in', '$2y$10$mEu3I7ZOV/v7xxrPNDnaWuWGRfztjDAylgsu69IWnkaFc8rnf51OC', '9874761964', '7379', '', 10.01, 11.20, 'India', 'جزيره العرب', 'ANDHRA PRADESH', 'جزيره العرب', 'Bodhan', 'جزيره العرب', 'CJ -159, Sector 2, Salt Lake City, Kolkata, West Bengal 700091', 'جزيره العربجزيره العربجزيره العربجزيره العربجزيره العربجزيره العربجزيره العربجزيره العربجزيره العرب', 1, 1, NULL, 'MTIzNDU2Nzg5MA==', '2019-09-10 07:25:58', '2019-09-20 02:00:52'),
(57, NULL, 'Ranu', NULL, 'Mondal', 'ranu.mondal@cogitosoftware.in', '$2y$10$.IdSIYmvDNUKnDnN/MKh5uSIbXpDmNO0x7M87SbipOp.cZ5830Nte', '7439272532', '7680', '', 10.01, 11.20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 'MTIzNDU2Nzg5MA==', '2019-09-11 07:30:23', '2019-09-11 07:30:23'),
(58, 'Rubina', 'الرميلة', 'Khatun', 'خاتون', 'ruma.mondal@gmail.com', '$2y$10$7LZ7KB4wPQEsMCRgh2f7FuYdTbmPxfj2qMNVXoxfKwJNrf855gSh2', '7439272533', '7655', '', 10.01, 11.20, 'arabia', 'جزيره العرب', 'al-ahat', 'ابي جامع', 'al-ahat', 'ابي جامع', 'sfsadfsfsadfasdfasdfasdfsadfsadfsa', 'قطاع بحيرة الملح', 1, 1, NULL, 'MTIzNDU2ODkw', '2019-09-11 07:35:26', '2019-09-11 09:05:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phonenumber_unique` (`phoneNumber`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
