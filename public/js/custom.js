var _basePath = "http://localhost/roi/public/";
$("#lang2").hide();
$("#lang_1").val(1);
$("#langSelector").change(function () {
    var lang_id = $('#langSelector').val();
    var is_parent = $("#is_parent").val();
    var _token = $("input[name='_token']").val();
    //alert(lang_id);

    if (lang_id == 1) {
        //alert('hi-English');
        $("#lang_" + lang_id).val(lang_id);
        $("#lang1").show();
        $("#lang2").hide();
        $.ajax({
            type: 'POST',
            url: _basePath + 'category_parent_edit',
            data: {
                '_token': _token,
                'lang_id': lang_id,
                'is_parent': is_parent
            },
            dataType: 'json',
            success: function (response) {
                if (response['process'] == 'success') {
                    //$("#myOverlay").hide();
                    //$("#loadingGIF").hide();  
                    $(".parent_cat_block_edit").html('');
                    $(".parent_cat_block_edit_ar").html('');
                    $(".parent_cat_block_edit").html(response['HTML']);

                } else {
                    alert('No respose');
                }

            },
            error: function (xhr, status, error) {
                alert(error);
            }
        })
    }
});

$("#langSelectorFront").change(function () {

    var lang_id = $('#langSelectorFront').val();
    var _token = $("input[name='_token']").val();
    //alert(_token);
    $.ajax({
        type: 'POST',
        url: _basePath + 'category_parent',
        data: {
            '_token': _token,
            'lang_id': lang_id
        },
        dataType: 'json',
        success: function (response) {
            if (response['process'] == 'success') {
                //$("#myOverlay").hide();
                //$("#loadingGIF").hide();  
                $(".parent_cat_block").html('');
                $(".parent_cat_block").html(response['HTML']);

            } else {
                alert('No respose');
            }

        },
        error: function (xhr, status, error) {
            alert(error);
        }
    })
});

$(document).ready(function () {
    //$("#langSelector").click();
    var lang_id = $('#langSelector').val();
    var is_parent = $("#is_parent").val();
    var _token = $("input[name='_token']").val();
    if (lang_id == 1) {
        //alert('hi-English');
        $("#lang_" + lang_id).val(lang_id);
        $("#lang1").show();
        $("#lang2").hide();
        $.ajax({
            type: 'POST',
            url: _basePath + 'category_parent_edit',
            data: {
                '_token': _token,
                'lang_id': lang_id,
                'is_parent': is_parent
            },
            dataType: 'json',
            success: function (response) {
                if (response['process'] == 'success') {
                    //$("#myOverlay").hide();
                    //$("#loadingGIF").hide();  

                    $(".parent_cat_block_edit").html('');
                    $(".parent_cat_block_edit_ar").html('');
                    $(".parent_cat_block_edit").html(response['HTML']);

                } else {
                    alert('No respose');
                }

            },
            error: function (xhr, status, error) {
                alert(error);
            }
        })
    }
})
