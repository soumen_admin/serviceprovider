<?php

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {    
	
	if(\Auth::check()){
		if(auth()->user()->firstName == 'admin')
        	return view('adminHome');
    	else
        	return view('home');
    }
    else
    	return view('welcome');
    
});

Route::post('/contact', 'frontend\MailController@sendMail')->name('contact');

// FRONT END ROUTE ........................................

Route::group(['middleware' => 'auth'], function () 
{
	Route::get('/checkout/{token}/{token1}', 'frontend\ProductController@checkoutDetails')->name('checkout');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'user', 'as' => 'user.'], function () 
{
	Route::get('/profile/{token}', 'frontend\UserController@profile')->name('profile');
	Route::get('/user-product/{token}', 'frontend\UserController@userproduct')->name('user-product');
	Route::get('/changepassword/{token}', 'frontend\UserController@changepassword')->name('changepassword');	
	Route::post('change', 'frontend\UserController@change')->name('change');
});

// FRONT END ROUTE ........................................

Route::get('/admin', function () {
    return view('admin.admin');
});


//ROUTE FOR LARAVEL FILE MANAGER ......................
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web','auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });
//ROUTE FOR LARAVEL FILE MANAGER ......................

Route::middleware(['auth'])->group(function () {

	Route::get('admin/password', function () {
	    return view('admin.reset');
	});

	Route::post('/adminpassword', 'AdminController@index')->name('admin_password');
	Route::get('/admin/profile', 'AdminController@profile')->name('admin_profile');
	

	Route::get('/user', 'UserController@index')->name('user');
	Route::get('/user_add', 'UserController@show')->name('user_add');
	Route::get('/user_edit/{id}', 'UserController@edit')->name('user_edit');
	Route::put('/user_update/{id}', 'UserController@update')->name('user_update');
	Route::post('/user_add', 'UserController@store')->name('user_add');
	Route::post('/user_delete{id}', 'UserController@delete')->name('user_delete');


	// BRANCHES -----------------------------------------------------------------
	Route::get('/branches', 'BranchesController@index')->name('branches');
	Route::get('/branches_add', 'BranchesController@show')->name('branches_add');
	Route::get('/branches_edit/{id}', 'BranchesController@edit')->name('branches_edit');
	Route::put('/branches_update/{id}', 'BranchesController@update')->name('branches_update');
	Route::post('/branches_add', 'BranchesController@store')->name('branches_add');
	Route::post('/branches_delete{id}', 'BranchesController@delete')->name('branches_delete');
	// BRANCHES -----------------------------------------------------------------

	// CLIENTS -----------------------------------------------------------------
	Route::get('/clients', 'ClientsController@index')->name('clients');
	Route::get('/clients_add', 'ClientsController@show')->name('clients_add');
	Route::get('/clients_edit/{id}', 'ClientsController@edit')->name('clients_edit');
	Route::put('/clients_update/{id}', 'ClientsController@update')->name('clients_update');
	Route::post('/clients_add', 'ClientsController@store')->name('clients_add');
	Route::post('/clients_delete{id}', 'ClientsController@delete')->name('clients_delete');
	// CLIENTS -----------------------------------------------------------------

	Route::resource('page', 'PageController');

	//product CONTROLLER ROUTE BACK END
		Route::resource('product', 'productController');				
	//product CONTROLLER ROUTE BACK END
    
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
