<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

	    for($i = 0; $i < 10; $i++) {
	        App\User::create([
	            'firstName' => $faker->firstName,
		        'lastName' => $faker->lastName,
		        'email' => $faker->unique()->safeEmail,
		        //'email_verified_at' => now(),
		        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
		        'phoneNumber' => Str::random(10),
		        'image' => $faker->image('images',400,300, null, false),
		        //'latitude' => Str::random(10),
		        //'longitude' => Str::random(10),
		        'country' => $faker->country,
		        'state' => $faker->state,
		        'city' => $faker->city,
		        'address' => $faker->address,                
		        'remember_token' => Str::random(10)	            
	        ]);
	    }
    }
}
